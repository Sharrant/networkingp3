Start server and game client at same time (this may take a few tries)
WASD to move the ship you are placing around.
Space places the ship.

Once 5 ships have been placed they will try to fly past the enemy turrets and you will receive points based on the number and health of the survivors. The score is shown on the console after the game ends.

Client receives the turret positions from the game server.

The score board and REST pages are in netproj, but are not currently hooked up to the game client and server

in the ITD folder is a WIP of the game in unity (not currently connected to a game server)