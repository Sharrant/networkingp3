#include <glad/glad.h>		
#include <GLFW/glfw3.h>		
#include <iostream>

//#include "linmath.h"
#include <glm/vec3.hpp> // glm::vec3
#include <glm/vec4.hpp> // glm::vec4
#include <glm/mat4x4.hpp> // glm::mat4
#include <glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective
#include <glm/gtc/type_ptr.hpp> // glm::value_ptr
#include <glm\gtc\random.hpp>
#include <stdlib.h>
#include <stdio.h>
#include <sstream>		// "String Stream", used for setting the windows title
#include "json.hpp"
#include <vector>
#include "cGameObject.h"
#include <locale>
#include "cMeshTypeManager.h"	
#include "cShaderManager.h"
#include "vert_XYZ_RGB.h"

#include "cContact.h"	// Object collision object

#include "Utilities.h"

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <string>
#include <thread>

#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

#include "Buffer.h"
using json = nlohmann::json;

#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "27015"
struct in_addr ipv4addr;
std::vector< cGameObject* > g_vec_pGOs;
std::vector< cContact > g_vec_Contacts;
std::vector< cGameObject* > g_vec_pClosestTurret;
bool gameDataLoaded = false;
cMeshTypeManager* g_pTheMeshTypeManager = 0;

cShaderManager* g_pTheShaderManager = 0;
void ReadIncomingMessage(Buffer* receiveBuffer);
void ReceiveMessages(SOCKET ConnectSocket, Buffer* sendBuffer, Buffer* receiveBuffer, char recvbuf[], int recvbuflen);
void ComposeMessage(int gameID, int mode, Buffer* sendBuffer);
void PhysicsStep(float deltaTime);
void CollisionStep(float deltaTime);
void TurretDetection();
void ReleaseBomb();
void LoadTurrets();
void Networking();
void FinalizePositions();
bool LoadObjectsIntoScene( std::vector< cGameObject* > &vec_pGOs );

// We should place this into a class...
//Default
float g_CameraX = 50.f;
float g_CameraY = 50.f;
float g_CameraZ = 0;// -25.f;
float g_CameraMovementSpeed = 0.4f;
std::string userName = "";
int score = 0;

bool turret1LockedOn = false;
bool turret2LockedOn = false;
bool turret3LockedOn = false;
//Luke default light camera
//float g_CameraX = 25.f;
//float g_CameraY = 1.f;
//float g_CameraZ = 0.0f;
//float g_CameraMovementSpeed = 0.1f;

//pLuke->x = 0;		// getRandFloat(-30.0f, 30.0f);
//pLuke->y = 0;		// getRandFloat(-30.0f, 30.0f);
//pLuke->z = -1.1;		// getRandFloat(-30.0f, 30.0f);

float g_TargetX = 0.0f;
float g_TargetY = 1.f;
float g_TargetZ =0.f;

glm::vec3 lightPos(1.f, 2.f, 10.f);		// I just picked these values after moving the light around until it looked "nice"
float lightConstAtten = 0.0f;
float lightLinearAtten = 0.0f;
float lightQuadAtten = 0.001f;


glm::vec3 lightPos1(7.f, 2.f, 25.f);		// I just picked these values after moving the light around until it looked "nice"
float lightConstAtten1 = 0.0f;
float lightLinearAtten1 = 0.0f;
float lightQuadAtten1 = 0.0265429f;

glm::vec3 lightPos2(0.f, 20.f, 0.f);		// I just picked these values after moving the light around until it looked "nice"
float lightConstAtten2 = 0.0f;
float lightLinearAtten2 = 0.0f;
float lightQuadAtten2 = 0.0f;




glm::vec3 lightPos3(.0024f, 0.0022f, -1.09f);		// I just picked these values after moving the light around until it looked "nice"
float lightConstAtten3 = 10.f;
glm::vec4 lightDiffuse3(1.f, 0.f, 0.f, 1.f);
float lightLinearAtten3 = 0.241427f;
float lightQuadAtten3 = 100000000.f;



glm::vec3 lightPos4(-.0024f, 0.0022f, -1.09f);		// I just picked these values after moving the light around until it looked "nice"
float lightConstAtten4 = 0.0f;
glm::vec4 lightDiffuse4(1.f, 0.f, 0.f, 1.f);
float lightLinearAtten4 = 0.0f;
float lightQuadAtten4 = 0.0f;


glm::vec3 lightPos5(.0023f, -0.0024f, -1.09f);		// I just picked these values after moving the light around until it looked "nice"
float lightConstAtten5 = 0.0f;
glm::vec4 lightDiffuse5(1.f, 0.f, 0.f, 1.f);
float lightLinearAtten5 = 0.0f;
float lightQuadAtten5 = 0.0f;

glm::vec3 lightPos6(-.0023f, 0.0024f, -1.09f);		// I just picked these values after moving the light around until it looked "nice"
float lightConstAtten6 = 0.0f;
glm::vec4 lightDiffuse6(1.f, 0.f, 0.f, 1.f);
float lightLinearAtten6 = 0.0f;
float lightQuadAtten6 = 0.0f;


glm::vec3 lightPos7(-.0023f, 0.0024f, -0.9f);		// I just picked these values after moving the light around until it looked "nice"
float lightConstAtten7 = 0.0f;
glm::vec4 lightDiffuse7(0.f, 1.f, 0.f, 1.f);
float lightLinearAtten7 = 0.0f;
float lightQuadAtten7 = 0.0f;



glm::vec3 lightPos8(-.0023f, 0.0124f, -1.f);		// I just picked these values after moving the light around until it looked "nice"
float lightConstAtten8 = 0.0f;
glm::vec4 lightDiffuse8(0.f, 1.f, 0.f, 1.f);
float lightLinearAtten8 = 0.0f;
float lightQuadAtten8 = 0.0f;

glm::vec3 lightPos9(-0.17f, 0.15, -.8f);		// I just picked these values after moving the light around until it looked "nice"
float lightConstAtten9 = 0.0f;
glm::vec4 lightDiffuse9(1.f, 0.f, 0.f, 1.f);
float lightLinearAtten9 = 0.0f;
float lightQuadAtten9 = 0.0f;


//
//glm::vec3 lightPos4(-17.f, 4.f, 10.f);		// I just picked these values after moving the light around until it looked "nice"
//float lightConstAtten4 = 0.0f;
//float lightLinearAtten4 = 0.141427f;
//float lightQuadAtten4 = 0.0265429f;
//
//
//glm::vec3 lightPos5(0.f, 0.f, 2.f);		// I just picked these values after moving the light around until it looked "nice"
//float lightConstAtten5 = 0.0f;
//glm::vec4 lightDiffuse5(1.f, 0.f, 0.f,1.f);
//float lightLinearAtten5 = 0.141427f;
//float lightQuadAtten5 = 0.0265429f;
//
//glm::vec3 lightPos5(0.f, 0.f, 2.f);		// I just picked these values after moving the light around until it looked "nice"
//float lightConstAtten5 = 0.0f;
//glm::vec4 lightDiffuse5(1.f, 0.f, 0.f, 1.f);
//float lightLinearAtten5 = 0.141427f;
//float lightQuadAtten5 = 0.0265429f;

glm::vec3 turret1;
glm::vec3 turret2;
glm::vec3 turret3;


bool bDrawLightDebugSpheres = true;
bool bDrawLightDebugLocation = true;

// This will be drawn multiple times... 
unsigned int g_DebugSphereID = 0;
int placementTimer = 0;

//static const struct
//{
//    float x, y;
//    float r, g, b;
//} vertices[6] =
struct vertXYRGB
{
    float x, y;				// 4 + 4 
    float r, g, b;			// 4 + 4 + 4  = 20 bytes
};

int shipSelected = 0;
bool shipSetup = true;

//struct vert_XYZ_RGB
//{
//    float x, y, z;				// 4 + 4 + 4
//    float r, g, b;			// 4 + 4 + 4  = 24 bytes
//};


static void error_callback(int error, const char* description)
{
    fprintf(stderr, "Error: %s\n", description);
}

bool isShiftDownAlone( int mods )
{
	if ( (mods & GLFW_MOD_SHIFT) == GLFW_MOD_SHIFT )
	{
		return true;
	}
	return false;
}

bool isCrtlDownAlone( int mods )
{
	if ( (mods & GLFW_MOD_CONTROL) == GLFW_MOD_CONTROL )
	{
		return true;
	}
	return false;
}

bool isAltDownAlone( int mods )
{
	if ( (mods & GLFW_MOD_ALT) == GLFW_MOD_ALT )
	{
		return true;
	}
	return false;
}

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GLFW_TRUE);



	switch (key)
	{
	case GLFW_KEY_Q:
		::g_vec_pGOs[0]->y += g_CameraMovementSpeed;
		//::g_CameraY -= g_CameraMovementSpeed;		// "Down"
		break;
	case GLFW_KEY_E:
		::g_vec_pGOs[0]->y -= g_CameraMovementSpeed;
		//::g_CameraY += g_CameraMovementSpeed;		// "Up"
		break;
	case GLFW_KEY_W:
		if (shipSetup)
			::g_vec_pGOs[shipSelected]->x = ::g_vec_pGOs[shipSelected]->x - .2f;
		//::g_CameraY += g_CameraMovementSpeed;		// "Up"
		break;
	case GLFW_KEY_A:
		if (shipSetup)
		::g_vec_pGOs[shipSelected]->z = ::g_vec_pGOs[shipSelected]->z + .2f;
		//::g_CameraY += g_CameraMovementSpeed;		// "Up"
		break;
	case GLFW_KEY_S:

		//::g_CameraY += g_CameraMovementSpeed;		// "Up"
		if (shipSetup)
			::g_vec_pGOs[shipSelected]->x = ::g_vec_pGOs[shipSelected]->x + .2f;
		break;
	case GLFW_KEY_D:
		if (shipSetup)
		::g_vec_pGOs[shipSelected]->z = ::g_vec_pGOs[shipSelected]->z - .2f;
		//::g_CameraY += g_CameraMovementSpeed;		// "Up"
		break;
	case GLFW_KEY_SPACE:
		//ReleaseBomb();
		if (shipSelected < 4)
		{
			if (placementTimer < 2)
			{
				shipSelected++;
				::g_vec_pGOs[shipSelected]->bIsVisible = true;
				placementTimer = 30;
			}
			::g_vec_pGOs[0]->bIsUpdatedByPhysics = false;
			::g_vec_pGOs[1]->bIsUpdatedByPhysics = false;
			::g_vec_pGOs[2]->bIsUpdatedByPhysics = false;
			::g_vec_pGOs[3]->bIsUpdatedByPhysics = false;
			::g_vec_pGOs[4]->bIsUpdatedByPhysics = false;
		}
		else
		{
			
			::g_vec_pGOs[0]->bIsUpdatedByPhysics = true;
			::g_vec_pGOs[1]->bIsUpdatedByPhysics = true;
			::g_vec_pGOs[2]->bIsUpdatedByPhysics = true;
			::g_vec_pGOs[3]->bIsUpdatedByPhysics = true;
			::g_vec_pGOs[4]->bIsUpdatedByPhysics = true;
			shipSetup = false;
			FinalizePositions();
		}
		break;
	}// Update the camera position in the title...
//	glfwSetWindowTitle( window, "Sexy" );

	/*std::stringstream ssTitle;
	ssTitle << "Camera: " 
		<< ::g_CameraX << ", "
		<< ::g_CameraY << ", "
		<< ::g_CameraZ
		<< "  Light: " 
		<< ::lightPos.x << ", " 
		<< ::lightPos.y << ", " 
		<< ::lightPos.z
		<< " Lin: " << ::lightLinearAtten
		<< " Quad: " << ::lightQuadAtten;
	glfwSetWindowTitle( window, ssTitle.str().c_str() );
*/

	return;
}



// Search by ID, then returns pointer to object
cGameObject* findObjectByID( unsigned int ID )
{
	for ( int index = 0; index != g_vec_pGOs.size(); index++ )
	{
		if ( ::g_vec_pGOs[index]->getID() == ID )
		{	// Found it! 
			return ::g_vec_pGOs[index];
		}
	}
	// Didn't find it
	return 0;
}


GLint locID_matModel = -1;		// 
GLint locID_matView = -1;
GLint locID_matProj = -1;

GLint UniformLoc_ID_objectColour = -1;
GLint UniformLoc_ID_isWireframe = -1;
GLint UniformLoc_ID_bUseDebugDiffuseNoLighting = -1;




void DrawObject( cGameObject* pCurGO )
{
	if ( ! pCurGO->bIsVisible )
	{
		// Don't draw it
		return;
	}

	std::string meshModelName = pCurGO->meshName;
			
	GLuint VAO_ID = 0;
	int numberOfIndices = 0;
	float unitScale = 1.0f;
	if ( ! ::g_pTheMeshTypeManager->LookUpMeshInfo( meshModelName,
													VAO_ID, 
													numberOfIndices,
													unitScale ) )
	{	// Skip the rest of the for loop, but continue
		return;
	}

//			glm::mat4x4 mvp(1.0f);		// Model, View, Projection matrix

//			mat4x4_identity(m);
	glm::mat4x4 matModel = glm::mat4x4(1.0f);


	// Pre-rotation
	// Translation
	// Post-rotation
	// Scale

	// Pre in that it's BEFORE the translation
	matModel = glm::rotate( matModel, pCurGO->pre_Rot_X, glm::vec3( 1.0f, 0.0f, 0.0f) );
	matModel = glm::rotate( matModel, pCurGO->pre_Rot_Y, glm::vec3( 0.0f, 1.0f, 0.0f) );
	matModel = glm::rotate( matModel, pCurGO->pre_Rot_Z, glm::vec3( 0.0f, 0.0f, 1.0f) );

	if (pCurGO->meshName == "imperial turret_N.ply")
	{
		matModel = glm::translate(matModel, glm::vec3(pCurGO->x, pCurGO->y, (pCurGO->z+2.f)));
	}
	else
	{
		matModel = glm::translate(matModel, glm::vec3(pCurGO->x, pCurGO->y, pCurGO->z));
	}



	// Pre in that it's AFTER the translation
	matModel = glm::rotate( matModel, pCurGO->post_Rot_X, glm::vec3( 1.0f, 0.0f, 0.0f) );
	matModel = glm::rotate( matModel, pCurGO->post_Rot_Y, glm::vec3( 0.0f, 1.0f, 0.0f) );
	matModel = glm::rotate( matModel, pCurGO->post_Rot_Z, glm::vec3( 0.0f, 0.0f, 1.0f) );

	//
	float actualScale = pCurGO->scale * unitScale;

	matModel = glm::scale(matModel, glm::vec3( actualScale, actualScale, actualScale ));


	if ( pCurGO->bIsWireframe )
	{	// Turn off backface culling
		// Enable "wireframe" polygon mode
		glPolygonMode( GL_FRONT_AND_BACK,	// GL_FRONT_AND_BACK is the only thing you can pass here
						GL_LINE );			// GL_POINT, GL_LINE, or GL_FILL
		glDisable( GL_CULL_FACE );	
	}
	else
	{	// "Regular" rendering: 
		// Turn on backface culling
		// Turn polygon mode to solid (Fill)
		glCullFace( GL_BACK );		// GL_FRONT, GL_BACK, or GL_FRONT_AND_BACK
		glEnable( GL_CULL_FACE );
		glPolygonMode( GL_FRONT_AND_BACK,	// GL_FRONT_AND_BACK is the only thing you can pass here
					   GL_FILL );			// GL_POINT, GL_LINE, or GL_FILL
	}

//			mat4x4_mul(mvp, p, m);
		// This is being calculated in the shader now...
//			mvp = p * v * m;

//			glUseProgram(program);
	::g_pTheShaderManager->useShaderProgram("simple");


	glUniformMatrix4fv( locID_matModel, 1, GL_FALSE, 
				        (const GLfloat*) glm::value_ptr(matModel) );



	// Setting the uniform colours
	glUniform4f(UniformLoc_ID_objectColour,
		//1.f, 1.f, 1.f, 0.f);
					pCurGO->solid_R, pCurGO->solid_G,  pCurGO->solid_B, 1.0f ); 
	if ( pCurGO->bIsWireframe )
	{
		glUniform1i( UniformLoc_ID_isWireframe, TRUE );		// 1
	}
	else 
	{
		glUniform1i( UniformLoc_ID_isWireframe, FALSE );	// 0
	}
	
	if ( pCurGO->bUseDebugColours )
	{
		glUniform1i( UniformLoc_ID_bUseDebugDiffuseNoLighting, TRUE );		// 1
	}
	else 
	{
		glUniform1i( UniformLoc_ID_bUseDebugDiffuseNoLighting, FALSE );	// 0
	}

	//glDrawArrays(GL_TRIANGLES, 0, 3);
	//glDrawArrays(GL_TRIANGLES, 0, numberofVerts);

	// Drawing indirectly from the index buffer

	glBindVertexArray( VAO_ID );
	glDrawElements( GL_TRIANGLES, 
					numberOfIndices, 
					GL_UNSIGNED_INT,	// Each index is how big?? 
					(GLvoid*) 0 );		// Starting point in buffer
	glBindVertexArray(0);

	return;
}

void DrawDebugBall( glm::vec3 position, 
				    glm::vec3 colour, float scale )
{
	// And even more drawing...
	cGameObject* pDebugBall = ::findObjectByID( ::g_DebugSphereID );

	// Save old state
	glm::vec3 oldPosition( pDebugBall->x, pDebugBall->y, pDebugBall->z );
	glm::vec3 oldColour( pDebugBall->solid_R, pDebugBall->solid_G, pDebugBall->solid_B );
	float oldScale = pDebugBall->scale;
	bool oldIsVisible = pDebugBall->bIsVisible;
	bool oldIsWireframe = pDebugBall->bIsWireframe;
	bool oldbUseDebugColours = pDebugBall->bUseDebugColours;

	// Set values
	pDebugBall->x = position.x; 
	pDebugBall->y = position.y;	
	pDebugBall->z = position.z;
	pDebugBall->solid_R = colour.x;
	pDebugBall->solid_G = colour.y;
	pDebugBall->solid_B = colour.z;
	pDebugBall->bIsWireframe = true;
	pDebugBall->bIsVisible = true;
	pDebugBall->bUseDebugColours = true;
	pDebugBall->scale = scale;

	DrawObject( pDebugBall );

	// Restore state...
	pDebugBall->x = oldPosition.x;
	pDebugBall->y = oldPosition.y;
	pDebugBall->z = oldPosition.z;
	pDebugBall->solid_R = oldColour.x;
	pDebugBall->solid_G = oldColour.y;
	pDebugBall->solid_B = oldColour.z;
	pDebugBall->scale = oldScale;
	pDebugBall->bIsVisible = oldIsVisible;
	pDebugBall->bIsWireframe = oldIsWireframe;
	pDebugBall->bUseDebugColours = oldbUseDebugColours;

	return;
}

void DrawDebugLightSpheres(void)
{
	// And even more drawing...
	cGameObject* pDebugBall = ::findObjectByID( ::g_DebugSphereID );

	// Draw HOT PINK ball where the light is...
	if ( ::bDrawLightDebugLocation )
	{
		DrawDebugBall( glm::vec3( ::lightPos.x, ::lightPos.y, ::lightPos.z ),
		               glm::vec3( 255.0f/255.0f, 105.0f/255.0f, 180.0f/255.0f ), 0.25f );
	}

	if ( ::bDrawLightDebugSpheres )
	{
		// Draw a red ball at 75% brightness
		float distanceAt75Percent = calcApproxDistFromAtten( 
						0.75f, // desired brighness
						::lightConstAtten, ::lightLinearAtten, ::lightQuadAtten, 
						0.0001f, // Accuracy (how close to 0.75 are we getting
						1000.0f );  // "Infinite" distance (so give up)
		DrawDebugBall( glm::vec3( ::lightPos.x, ::lightPos.y, ::lightPos.z ),
						glm::vec3( 1.0f, 0.0f, 0.0f), distanceAt75Percent );

		// Draw a yellow ball at 50% brightness
		float distanceAt50Percent = calcApproxDistFromAtten( 
						0.50f, // desired brighness
						::lightConstAtten, ::lightLinearAtten, ::lightQuadAtten, 
						0.0001f, // Accuracy (how close to 0.75 are we getting
						1000.0f );  // "Infinite" distance (so give up)
		DrawDebugBall( glm::vec3( ::lightPos.x, ::lightPos.y, ::lightPos.z ),
						glm::vec3( 1.0f, 1.0f, 0.0f), distanceAt50Percent );

		// Draw a green ball at 25% brightness
		float distanceAt25Percent = calcApproxDistFromAtten( 
						0.25f, // desired brighness
						::lightConstAtten, ::lightLinearAtten, ::lightQuadAtten, 
						0.0001f, // Accuracy (how close to 0.75 are we getting
						1000.0f );  // "Infinite" distance (so give up)
		DrawDebugBall( glm::vec3( ::lightPos.x, ::lightPos.y, ::lightPos.z ),
						glm::vec3( 0.0f, 1.0f, 0.0f), distanceAt25Percent );

		// Draw a green ball at 00% brightness
		float distanceAt01Percent = calcApproxDistFromAtten( 
						0.01f, // desired brighness
						::lightConstAtten, ::lightLinearAtten, ::lightQuadAtten, 
						0.0001f, // Accuracy (how close to 0.75 are we getting
						1000.0f );  // "Infinite" distance (so give up)
		DrawDebugBall( glm::vec3( ::lightPos.x, ::lightPos.y, ::lightPos.z ),
						glm::vec3( 0.0f, 1.0f, 1.0f), distanceAt01Percent );

	}//if ( ::bDrawLightDebugSpheres )

	return;
}

int main(void)
{
	
	std::cout << "Please enter a username" << std::endl;
	std::getline(std::cin, userName);
    GLFWwindow* window;
	// Added an "index_buffer" variable here
    //GLuint vertex_buffer, index_buffer, vertex_shader, fragment_shader, program;
	//GLuint vertex_shader, fragment_shader; //, program;
//    GLint mvp_location;//, vpos_location, vcol_location;

	//GLint locID_matModel = -1;		// 
	//GLint locID_matView = -1;
	//GLint locID_matProj = -1;

	GLint locID_lightPosition = -1;
	GLint locID_lightDiffuse = -1;
	GLint locID_lightAttenuation = -1;
	GLint locID_lightSpecular = -1;

	GLint locID_lightPosition1 = -1;
	GLint locID_lightDiffuse1 = -1;
	GLint locID_lightAttenuation1 = -1;
	GLint locID_lightSpecular1 = -1;

	GLint locID_lightPosition2 = -1;
	GLint locID_lightDiffuse2 = -1;
	GLint locID_lightAttenuation2 = -1;
	GLint locID_lightSpecular2 = -1;

	GLint locID_lightPosition3 = -1;
	GLint locID_lightDiffuse3 = -1;
	GLint locID_lightAttenuation3 = -1;
	GLint locID_lightSpecular3 = -1;

	GLint locID_lightPosition4 = -1;
	GLint locID_lightDiffuse4 = -1;
	GLint locID_lightAttenuation4 = -1;
	GLint locID_lightSpecular4 = -1;

	GLint locID_lightPosition5 = -1;
	GLint locID_lightDiffuse5 = -1;
	GLint locID_lightAttenuation5 = -1;
	GLint locID_lightSpecular5 = -1;


	GLint locID_lightPosition6 = -1;
	GLint locID_lightDiffuse6 = -1;
	GLint locID_lightAttenuation6 = -1;
	GLint locID_lightSpecular6 = -1;

	GLint locID_lightPosition7 = -1;
	GLint locID_lightDiffuse7 = -1;
	GLint locID_lightAttenuation7 = -1;
	GLint locID_lightSpecular7 = -1;


	GLint locID_lightPosition8 = -1;
	GLint locID_lightDiffuse8= -1;
	GLint locID_lightAttenuation8 = -1;
	GLint locID_lightSpecular8 = -1;

	GLint locID_lightPosition9 = -1;
	GLint locID_lightDiffuse9 = -1;
	GLint locID_lightAttenuation9 = -1;
	GLint locID_lightSpecular9 = -1;


    glfwSetErrorCallback(error_callback);

    if (!glfwInit())
        exit(EXIT_FAILURE);


    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    window = glfwCreateWindow(1200, 800, "Simple example", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    glfwSetKeyCallback(window, key_callback);
    glfwMakeContextCurrent(window);
    gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
    glfwSwapInterval(1);

	bool gameOver = false;

	std::cout << glGetString(GL_VENDOR) << " " 
		<< glGetString(GL_RENDERER) << ", " 
		<< glGetString(GL_VERSION) << std::endl;
	std::cout << "Shader language version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;

//	std::cout << glGetString(GL_EXTENSIONS) << std::endl;

	::g_pTheShaderManager = new cShaderManager();

	cShaderManager::cShader verShader;	
	cShaderManager::cShader fragShader;

	//verShader.fileName = "simpleVert.glsl";
	//fragShader.fileName = "simpleFrag.glsl";
	verShader.fileName = "phongVert.glsl";
	fragShader.fileName = "phongFrag.glsl";
	::g_pTheShaderManager->setBasePath( "assets//shaders//" );
	if ( ! ::g_pTheShaderManager->createProgramFromFile( "simple", verShader, fragShader ) )
	{	// Oh no!
		std::cout << ::g_pTheShaderManager->getLastError() << std::endl;
		std::cout.flush();
		// TODO: Shut it all down... 
		return -1;
	}



	// Shader loading happening before vertex buffer array
 //   vertex_shader = glCreateShader(GL_VERTEX_SHADER);

 ////  char* vertex_shader_text = "wewherlkherlkh";

 //   glShaderSource(vertex_shader, 1, &vertex_shader_text, NULL);
 //   glCompileShader(vertex_shader); //???? 

 //   fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
 //   glShaderSource(fragment_shader, 1, &fragment_shader_text, NULL);
 //   glCompileShader(fragment_shader);

 //   program = glCreateProgram();
 //   glAttachShader(program, vertex_shader);
 //   glAttachShader(program, fragment_shader);
 //   glLinkProgram(program);

	// Entire loading the model and placing ito the VAOs 
	// is now in the cMeshTypeManager
	::g_pTheMeshTypeManager = new cMeshTypeManager();


	// Note: this returns a bool, which you might want to check...
	// (We won't right now, though)
	GLuint shadProgID = ::g_pTheShaderManager->getIDFromFriendlyName("simple");
	::g_pTheMeshTypeManager->SetBaseFilePath( "assets//models//" );

	::g_pTheMeshTypeManager->ScaleEverythingToBoundingBoxOnLoad( true, 10.0f );

	::g_pTheMeshTypeManager->LoadPlyFileIntoGLBuffer( shadProgID, "Trench_export_2.ply");
	::g_pTheMeshTypeManager->LoadPlyFileIntoGLBuffer(shadProgID, "y-wing_N.ply");
	::g_pTheMeshTypeManager->LoadPlyFileIntoGLBuffer(shadProgID, "Isoshphere_xyz_InvertedNormals.ply");
	::g_pTheMeshTypeManager->LoadPlyFileIntoGLBuffer(shadProgID, "imperial turret_N.ply");
	::g_pTheMeshTypeManager->LoadPlyFileIntoGLBuffer(shadProgID, "Isosphere_xyz_InvertedNormals.ply");
	
;


//	struct vertXYRGB
//{
//    float x, y (z);				// 4 + 4 
//    float r, g, b;			// 4 + 4 + 4  = 20 bytes
//};


	// Shader loading and building was here...

//    mvp_location = glGetUniformLocation(shadProgID, "MVP");


	// glGetActiveUniforms... 
	locID_matModel = glGetUniformLocation(shadProgID, "matModel");
	locID_matView = glGetUniformLocation(shadProgID, "matView");
	locID_matProj = glGetUniformLocation(shadProgID, "matProj");


	locID_lightPosition = glGetUniformLocation(shadProgID, "theLights[0].position");
	locID_lightDiffuse = glGetUniformLocation(shadProgID, "theLights[0].diffuse");
	locID_lightAttenuation = glGetUniformLocation(shadProgID, "theLights[0].attenuation");// = 0.0f;		// Can set to 1.0 to "turn off" a light
	locID_lightSpecular = glGetUniformLocation(shadProgID, "theLights[0].specular");// = 0.0f;		// Can set to 1.0 to "turn off" a light


	locID_lightPosition1 = glGetUniformLocation(shadProgID, "theLights[1].position");
	locID_lightDiffuse1 = glGetUniformLocation(shadProgID, "theLights[1].diffuse");
	locID_lightAttenuation1 = glGetUniformLocation(shadProgID, "theLights[1].attenuation");// = 0.0f;		// Can set to 1.0 to "turn off" a light
	locID_lightSpecular1 = glGetUniformLocation(shadProgID, "theLights[1].specular");// = 0.0f;		// Can set to 1.0 to "turn off" a light

	locID_lightPosition2 = glGetUniformLocation(shadProgID, "theLights[2].position");
	locID_lightDiffuse2 = glGetUniformLocation(shadProgID, "theLights[2].diffuse");
	locID_lightAttenuation2 = glGetUniformLocation(shadProgID, "theLights[2].attenuation");// = 0.0f;		// Can set to 1.0 to "turn off" a light
	locID_lightSpecular2 = glGetUniformLocation(shadProgID, "theLights[2].specular");// = 0.0f;		// Can set to 1.0 to "turn off" a light

	locID_lightPosition3 = glGetUniformLocation(shadProgID, "theLights[3].position");
	locID_lightDiffuse3 = glGetUniformLocation(shadProgID, "theLights[3].diffuse");
	locID_lightAttenuation3 = glGetUniformLocation(shadProgID, "theLights[3].attenuation");// = 0.0f;		// Can set to 1.0 to "turn off" a light
	locID_lightSpecular3 = glGetUniformLocation(shadProgID, "theLights[3].specular");// = 0.0f;		// Can set to 1.0 to "turn off" a light

	locID_lightPosition4 = glGetUniformLocation(shadProgID, "theLights[4].position");
	locID_lightDiffuse4 = glGetUniformLocation(shadProgID, "theLights[4].diffuse");
	locID_lightAttenuation4 = glGetUniformLocation(shadProgID, "theLights[4].attenuation");// = 0.0f;		// Can set to 1.0 to "turn off" a light
	locID_lightSpecular4 = glGetUniformLocation(shadProgID, "theLights[4].specular");// = 0.0f;		// Can set to 1.0 to "turn off" a light

	locID_lightPosition5 = glGetUniformLocation(shadProgID, "theLights[5].position");
	locID_lightDiffuse5 = glGetUniformLocation(shadProgID, "theLights[5].diffuse");
	locID_lightAttenuation5 = glGetUniformLocation(shadProgID, "theLights[5].attenuation");// = 0.0f;		// Can set to 1.0 to "turn off" a light
	locID_lightSpecular5 = glGetUniformLocation(shadProgID, "theLights[5].specular");// = 0.0f;		// Can set to 1.0 to "turn off" a light

	locID_lightPosition6 = glGetUniformLocation(shadProgID, "theLights[6].position");
	locID_lightDiffuse6 = glGetUniformLocation(shadProgID, "theLights[6].diffuse");
	locID_lightAttenuation6 = glGetUniformLocation(shadProgID, "theLights[6].attenuation");// = 0.0f;		// Can set to 1.0 to "turn off" a light
	locID_lightSpecular6 = glGetUniformLocation(shadProgID, "theLights[6].specular");// = 0.0f;		// Can set to 1.0 to "turn off" a light


	locID_lightPosition7 = glGetUniformLocation(shadProgID, "theLights[7].position");
	locID_lightDiffuse7 = glGetUniformLocation(shadProgID, "theLights[7].diffuse");
	locID_lightAttenuation7 = glGetUniformLocation(shadProgID, "theLights[7].attenuation");// = 0.0f;		// Can set to 1.0 to "turn off" a light
	locID_lightSpecular7 = glGetUniformLocation(shadProgID, "theLights[7].specular");// = 0.0f;		// Can set to 1.0 to "turn off" a light


	locID_lightPosition8 = glGetUniformLocation(shadProgID, "theLights[8].position");
	locID_lightDiffuse8 = glGetUniformLocation(shadProgID, "theLights[8].diffuse");
	locID_lightAttenuation8 = glGetUniformLocation(shadProgID, "theLights[8].attenuation");// = 0.0f;		// Can set to 1.0 to "turn off" a light
	locID_lightSpecular8 = glGetUniformLocation(shadProgID, "theLights[8].specular");// = 0.0f;		// Can set to 1.0 to "turn off" a light

	locID_lightPosition9 = glGetUniformLocation(shadProgID, "theLights[9].position");
	locID_lightDiffuse9 = glGetUniformLocation(shadProgID, "theLights[9].diffuse");
	locID_lightAttenuation9 = glGetUniformLocation(shadProgID, "theLights[9].attenuation");// = 0.0f;		// Can set to 1.0 to "turn off" a light
	locID_lightSpecular9 = glGetUniformLocation(shadProgID, "theLights[9].specular");// = 0.0f;		// Can set to 1.0 to "turn off" a light



	if ( ! LoadObjectsIntoScene( ::g_vec_pGOs ) )
	{
		std::cout << "WARNING: Could not load all models into the scene." << std::endl;
	}

	std::thread NetworkingThread(Networking);
	//LoadTurrets();
	while (!gameDataLoaded)
	{

	}
	LoadTurrets();


//	LoadTurrets();

	//GLuint UniformLoc_ID_objectColour = 0;
	//GLuint UniformLoc_ID_isWireframe = 0;

	UniformLoc_ID_objectColour = glGetUniformLocation( shadProgID, "objectColour" );
	UniformLoc_ID_isWireframe = glGetUniformLocation( shadProgID, "isWireframe" );
	UniformLoc_ID_bUseDebugDiffuseNoLighting = glGetUniformLocation( shadProgID, "bUseDebugDiffuseNoLighting" );

	// Note that GLFW would have had to create a "depth buffer" also
	// (which it does, apparently...)
	glEnable(GL_DEPTH_TEST);

	

	std::cout << "Turret 1:" << turret1.x << " : " << turret1.y << " : " << turret1.z << std::endl;

    while (!glfwWindowShouldClose(window))
    {

		float ratio;
        int width, height;
//        mat4x4 m, p, mvp;
		glm::mat4x4 matProjection;		// Was just "p"

		glfwGetFramebufferSize(window, &width, &height);
        ratio = width / (float) height;

        glViewport(0, 0, width, height);

		TurretDetection();

		if (g_vec_pGOs[0]->z < -30.f)
		{
		//	g_vec_pGOs[0]->x = 30.f;
			if (g_vec_pGOs[0]->bIsUpdatedByPhysics)
			{
				g_vec_pGOs[0]->bIsUpdatedByPhysics = false;
				score += g_vec_pGOs[0]->health*10.f;
			}

		}

		if (g_vec_pGOs[1]->z < -30.f)
		{
			//	g_vec_pGOs[0]->x = 30.f;
			if (g_vec_pGOs[1]->bIsUpdatedByPhysics)
			{
				g_vec_pGOs[1]->bIsUpdatedByPhysics = false;
				score += g_vec_pGOs[1]->health*10.f;
			}
		}

		if (g_vec_pGOs[2]->z < -30.f)
		{
			//	g_vec_pGOs[0]->x = 30.f;
		

			if (g_vec_pGOs[2]->bIsUpdatedByPhysics)
			{
				g_vec_pGOs[2]->bIsUpdatedByPhysics = false;
				score += g_vec_pGOs[2]->health*10.f;
			}
		}

		if (g_vec_pGOs[3]->z < -30.f)
		{
			//	g_vec_pGOs[0]->x = 30.f;
			if (g_vec_pGOs[3]->bIsUpdatedByPhysics)
			{
				g_vec_pGOs[3]->bIsUpdatedByPhysics = false;
				score += g_vec_pGOs[3]->health*10.f;
			}
		}

		if (g_vec_pGOs[4]->z < -30.f)
		{
			//	g_vec_pGOs[0]->x = 30.f;
			if (g_vec_pGOs[4]->bIsUpdatedByPhysics)
			{
				g_vec_pGOs[4]->bIsUpdatedByPhysics = false;
				score += g_vec_pGOs[4]->health*10.f;
			}
		}

		if (!shipSetup && !gameOver&& !( g_vec_pGOs[4]->bIsUpdatedByPhysics || g_vec_pGOs[3]->bIsUpdatedByPhysics || g_vec_pGOs[2]->bIsUpdatedByPhysics || g_vec_pGOs[1]->bIsUpdatedByPhysics || g_vec_pGOs[0]->bIsUpdatedByPhysics))
		{
			std::cout << "Game over!" << std::endl;
			std::cout << "You got " << score << " points!" << std::endl;
			gameOver = true;




			WSADATA wsaData;
			SOCKET Socket;
			SOCKADDR_IN SockAddr;
			int lineCount = 0;
			int rowCount = 0;
			struct hostent *host;
			std::locale local;
			char buffer[10000];
			int i = 0;
			int nDataLength;
			std::string website_HTML;

			// website url
			std::string url = "amazonaws.com";// :8080 / ITD / webresources / records";
			//std::string url = "google.ca";
			std::string fullUrl = "ec2-54-203-10-190.us-west-2.compute.amazonaws.com:8080/ITD/webresources/records";
			//HTTP GET
			std::string get_http = "GET / HTTP/1.1\r\nHost: " + fullUrl + "\r\nConnection: close\r\n\r\n";


			if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) {
				std::cout << "WSAStartup failed.\n";
				system("pause");
				//return 1;
			}
			//const char *ipstr = "54.203.10.190";
			//struct in_addr ip;
			//SOCKADDR_IN sa;
			//inet_pton(AF_INET, "54.203.10.190", &(sa.sin_addr));


			Socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

			//host = gethostbyaddr("54.203.10.190:8080", sizeof ipv4addr, AF_INET);
			host = gethostbyname(url.c_str());//he = gethostbyaddr(&ipv4addr, sizeof ipv4addr, AF_INET);
	//		host = gethostbyaddress(url.c_str());
			SockAddr.sin_port = htons(80);
			SockAddr.sin_family = AF_INET;
			SockAddr.sin_addr.s_addr = *((unsigned long*)host->h_addr);

			if (connect(Socket, (SOCKADDR*)(&SockAddr), sizeof(SockAddr)) != 0) {
				std::cout << "Could not connect";
				system("pause");
				//return 1;
			}
			json j;
			j["username"] = userName;
			j["score"] = score;
			// send GET / HTTP
			send(Socket, get_http.c_str(), strlen(get_http.c_str()), 0);

			// recieve html
			while ((nDataLength = recv(Socket, buffer, 10000, 0)) > 0) {
				int i = 0;
				while (buffer[i] >= 32 || buffer[i] == '\n' || buffer[i] == '\r') {

					website_HTML += buffer[i];
					i += 1;
				}
			}
			

			closesocket(Socket);
			WSACleanup();

			// Display HTML source 
			std::cout << website_HTML;

			// pause
			std::cout << "\n\nPress ANY key to close.\n\n";
			std::cin.ignore(); std::cin.get();


			return 0;
		

		}


		// 1st one clears the "colour" buffer (i.e. the colour of the pixels)
		// 2nd one clears the "depth" or "Z" buffer 
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

		// The "view" matrix is (usually) only updated once per "scene"
		// The "projection" matrix only needs to be updated when the window size 
		//    changes, but here we are updating it every "scene"
		// (a "scene" is when ALL the game objects are drawn)

		glm::mat4x4 matView(1.0f);	// "View" (or camera) matrix

//		mat4x4_ortho(p, -ratio, ratio, -1.f, 1.f, 1.f, -1.f);
		matProjection = glm::perspective( 0.6f, ratio, 0.01f, 10000.0f );

		//v = glm::lookAt( glm::vec3( 0.0f, 0.0f, -10.0f), // Eye
		matView = glm::lookAt( 
			             glm::vec3( ::g_CameraX, ::g_CameraY, ::g_CameraZ ), // Eye
						 glm::vec3( ::g_TargetX, ::g_TargetY, ::g_TargetZ ), // At (target)
						 glm::vec3( 0.0f, 1.0f, 0.0f ) );	// Up

		// Physics update
		PhysicsStep( 0.01f );		// 10 ms
		// Collision detection step 
		CollisionStep(0.01f);

		if (placementTimer > 0)
			placementTimer--;
		// update the bunny rotation
//		::g_vec_pGOs[0]->pre_Rot_X += 0.01f;
//		::g_vec_pGOs[0]->post_Rot_X += 0.01f;

		// Only needs to be set if window is created and-or 
		// is resized (you could put this in the window re-size
		// callback in GLFW or freeGLUT)
		glUniformMatrix4fv(locID_matProj, 1, GL_FALSE, 
				    (const GLfloat*) glm::value_ptr(matProjection) );

		// This is set once at the start of the "scene" draw.
		glUniformMatrix4fv(locID_matView, 1, GL_FALSE, 
				    (const GLfloat*) glm::value_ptr(matView) );

		glUniform4f( locID_lightPosition, lightPos.x, lightPos.y, lightPos.z, 1.0f );
		glUniform4f( locID_lightDiffuse, 1.f,1.f,1.f, 1.0f );
		glUniform4f( locID_lightAttenuation, ::lightConstAtten, ::lightLinearAtten, ::lightQuadAtten, 1.0f );

		glUniform4f(locID_lightPosition1, lightPos1.x, lightPos1.y, lightPos1.z, 1.0f);
		glUniform4f(locID_lightDiffuse1, 1.f, 1.f, 1.f, 1.0f);
		glUniform4f(locID_lightAttenuation1, ::lightConstAtten1, ::lightLinearAtten1, ::lightQuadAtten1, 1.0f);

		glUniform4f(locID_lightPosition2, lightPos2.x, lightPos2.y, lightPos2.z, 1.0f);
		glUniform4f(locID_lightDiffuse2, 1.f, 1.f, 1.f, 1.0f);
		glUniform4f(locID_lightAttenuation2, ::lightConstAtten2, ::lightLinearAtten2, ::lightQuadAtten2, 1.0f);

		glUniform4f(locID_lightPosition3, lightPos3.x, lightPos3.y, lightPos3.z, 1.0f);
		glUniform4f(locID_lightDiffuse3, lightDiffuse3.x, lightDiffuse3.y, lightDiffuse3.z, lightDiffuse3.w);
		glUniform4f(locID_lightAttenuation3, ::lightConstAtten3, ::lightLinearAtten3, ::lightQuadAtten3, 1.0f);

		glUniform4f(locID_lightPosition4, lightPos4.x, lightPos4.y, lightPos4.z, 1.0f);
		glUniform4f(locID_lightDiffuse4, lightDiffuse4.x, lightDiffuse4.y, lightDiffuse4.z, lightDiffuse4.w);
		glUniform4f(locID_lightAttenuation4, ::lightConstAtten4, ::lightLinearAtten4, ::lightQuadAtten4, 1.0f);

		glUniform4f(locID_lightPosition5, lightPos5.x, lightPos5.y, lightPos5.z, 1.0f);
		glUniform4f(locID_lightDiffuse5, lightDiffuse5.x, lightDiffuse5.y, lightDiffuse5.z, lightDiffuse5.w);//::lightDiffuse5.x, ::lightDiffuse5.y, ::lightDiffuse5.z, ::lightDiffuse5.w);
		glUniform4f(locID_lightAttenuation5, ::lightConstAtten5, ::lightLinearAtten5, ::lightQuadAtten5, 1.0f);

		glUniform4f(locID_lightPosition6, lightPos6.x, lightPos6.y, lightPos6.z, 1.0f);
		glUniform4f(locID_lightDiffuse6, lightDiffuse6.x, lightDiffuse6.y, lightDiffuse6.z, lightDiffuse6.w);//::lightDiffuse5.x, ::lightDiffuse5.y, ::lightDiffuse5.z, ::lightDiffuse5.w);
		glUniform4f(locID_lightAttenuation6, ::lightConstAtten6, ::lightLinearAtten6, ::lightQuadAtten6, 1.0f);


glUniform4f(locID_lightPosition7, lightPos7.x, lightPos7.y, lightPos7.z, 1.0f);
glUniform4f(locID_lightDiffuse7, lightDiffuse7.x, lightDiffuse7.y, lightDiffuse7.z, lightDiffuse7.w);//::lightDiffuse5.x, ::lightDiffuse5.y, ::lightDiffuse5.z, ::lightDiffuse5.w);
glUniform4f(locID_lightAttenuation7, ::lightConstAtten7, ::lightLinearAtten7, ::lightQuadAtten7, 1.0f);


glUniform4f(locID_lightPosition8, lightPos8.x, lightPos8.y, lightPos8.z, 1.0f);
glUniform4f(locID_lightDiffuse8, lightDiffuse8.x, lightDiffuse8.y, lightDiffuse8.z, lightDiffuse8.w);//::lightDiffuse5.x, ::lightDiffuse5.y, ::lightDiffuse5.z, ::lightDiffuse5.w);
glUniform4f(locID_lightAttenuation8, ::lightConstAtten8, ::lightLinearAtten8, ::lightQuadAtten8, 1.0f);

glUniform4f(locID_lightPosition9, lightPos9.x, lightPos9.y, lightPos9.z, 1.0f);
glUniform4f(locID_lightDiffuse9, lightDiffuse9.x, lightDiffuse9.y, lightDiffuse9.z, lightDiffuse9.w);//::lightDiffuse5.x, ::lightDiffuse5.y, ::lightDiffuse5.z, ::lightDiffuse5.w);
glUniform4f(locID_lightAttenuation9, ::lightConstAtten9, ::lightLinearAtten9, ::lightQuadAtten9, 1.0f);

// Start of Draw Scene
for (int index = 0; index != ::g_vec_pGOs.size(); index++)
{
	cGameObject* pCurGO = ::g_vec_pGOs[index];

	// Draw an object.... 
	DrawObject(pCurGO);

}// for ( int index = 0;.... (bottom of "render scene" loop)
// End of Draw Scene

		// *********************************
	//	DrawDebugLightSpheres();
		// *********************************


		// Show or "present" what we drew...
glfwSwapBuffers(window);

glfwPollEvents();
	}

	// Bye bye...
	delete ::g_pTheMeshTypeManager;

	glfwDestroyWindow(window);
	glfwTerminate();
	exit(EXIT_SUCCESS);
}

float distanceBetweenSpheres(cGameObject* pBallA, cGameObject* pBallB)
{
	float deltaX = pBallA->x - pBallB->x;
	float deltaY = pBallA->y - pBallB->y;
	float deltaZ = pBallA->z - pBallB->z;

	return sqrt(deltaX*deltaX + deltaY*deltaY + deltaZ*deltaZ);

}

// Narrow phase 
bool testSphereSphereCollision(cGameObject* pBallA, cGameObject* pBallB)
{
	float totalRadius = pBallA->radius + pBallB->radius;
	if (distanceBetweenSpheres(pBallA, pBallB) <= totalRadius)
	{
		return true;
	}
	return false;
}


//void TurretDetection()
//{
//	g_vec_pGOs;
//	g_vec_pClosestTurret.clear();
//	int closestObjects[10] = { 0 };
//	float closestObjectsDist[10] = { 0 };
//
//
//
//	int count = 0;
//	bool match = false;
//
//
//	for (int index = 0; index != ::g_vec_pGOs.size(); index++)
//	{
//		if (g_vec_pGOs[index]->meshName == "imperial turret_N.ply")
//		{
//
//
//
//			glm::vec3 yWingPositiion(g_vec_pGOs[0]->x, g_vec_pGOs[0]->y, g_vec_pGOs[0]->z);
//			glm::vec3 turretPosition(g_vec_pGOs[index]->x, g_vec_pGOs[index]->y, g_vec_pGOs[index]->z);
//			float dist = glm::distance(yWingPositiion, turretPosition);
//			if (count == 0)
//			{
//				closestObjects[0] = index;
//				closestObjectsDist[0] = abs(dist);
//				count++;
//			}
//			else
//			{
//				for (int i = 0; i < count; i++)
//				{
//					if (abs(closestObjectsDist[i]) > abs(dist))
//					{
//						for (int j = count; j > i; j--)
//						{
//							closestObjectsDist[j] = closestObjectsDist[j - 1];
//							closestObjects[j] = closestObjects[j - 1];
//						}
//						closestObjects[i] = index;
//						closestObjectsDist[i] = dist;
//						if (count < 9)
//						{
//							count++;
//						}
//						break;
//					}
//				}
//			}
//
//		}
//	}
//	count = 0;
//	for (int index = 0; index != ::g_vec_pGOs.size(); index++)
//	{
//		if (g_vec_pGOs[index]->meshName == "Isoshphere_xyz_InvertedNormals.ply" && !g_vec_pGOs[index]->bIsUpdatedByPhysics)
//		{
//			if (count == 0)
//			{
//				g_vec_pGOs[index]->solid_B = 0.f;
//				g_vec_pGOs[index]->solid_G = 1.f;
//				g_vec_pGOs[index]->solid_R = 1.f;
//			}
//			else
//			{
//				g_vec_pGOs[index]->solid_B = 0.f;
//				g_vec_pGOs[index]->solid_G = 1.f;
//				g_vec_pGOs[index]->solid_R = 0.f;
//			}
//			g_vec_pGOs[index]->x = g_vec_pGOs[closestObjects[count]]->x;
//			g_vec_pGOs[index]->y = g_vec_pGOs[closestObjects[count]]->y+0.2f;
//			g_vec_pGOs[index]->z = g_vec_pGOs[closestObjects[count]]->z+0.2f;
//			count++;
//		}
//	}
//
//}

void TurretDetection()
{
	for (int i = 0; i < 5; i++)
	{
		if (::g_vec_pGOs[i]->health < 1)
		{
			::g_vec_pGOs[i]->bIsUpdatedByPhysics = false;
		}
		
		
		if (glm::distance(glm::vec3(::g_vec_pGOs[i]->x, ::g_vec_pGOs[i]->y, ::g_vec_pGOs[i]->z), turret1) < 5.f && ::g_vec_pGOs[i]->health > 0)
		{
			if (!turret1LockedOn)
			{
				::g_vec_pGOs[i]->health--;
				turret1LockedOn = true;
				if (::g_vec_pGOs[i]->health < 1)
				{
					::g_vec_pGOs[i]->bIsUpdatedByPhysics = false;
				}
			}
		}
		if (glm::distance(glm::vec3(::g_vec_pGOs[i]->x, ::g_vec_pGOs[i]->y, ::g_vec_pGOs[i]->z), turret2) < 5.f  && ::g_vec_pGOs[i]->health > 0)
		{
			if (!turret2LockedOn)
			{
				turret2LockedOn = true;
				::g_vec_pGOs[i]->health--;

				if (::g_vec_pGOs[i]->health < 1)
				{
					::g_vec_pGOs[i]->bIsUpdatedByPhysics = false;
				}
			}
		}
		if (glm::distance(glm::vec3(::g_vec_pGOs[i]->x, ::g_vec_pGOs[i]->y, ::g_vec_pGOs[i]->z), turret3) < 5.f  && ::g_vec_pGOs[i]->health > 0)
		{
			if (!turret3LockedOn)
			{
				turret3LockedOn = true;
				::g_vec_pGOs[i]->health--;
				if (::g_vec_pGOs[i]->health < 1)
				{
					::g_vec_pGOs[i]->bIsUpdatedByPhysics = false;
				}
			}
		}

		::g_vec_pGOs[i]->solid_G = 1.0f - (25 - (::g_vec_pGOs[i]->health))*1.f;

		::g_vec_pGOs[i]->solid_R = 0.f + (25 - (::g_vec_pGOs[i]->health))*1.f;
	}

	//std::cout << "Ship 1 health: " << ::g_vec_pGOs[0]->health << std::endl;
	//std::cout << "Ship 2 health: " << ::g_vec_pGOs[1]->health << std::endl;
	//std::cout << "Ship 3 health: " << ::g_vec_pGOs[2]->health << std::endl;
	//std::cout << "Ship 4 health: " << ::g_vec_pGOs[3]->health << std::endl;
	//std::cout << "Ship 5 health: " << ::g_vec_pGOs[4]->health << std::endl;

	turret1LockedOn = false;
	turret2LockedOn = false;
	turret3LockedOn = false;
}

void CollisionStep(float deltaTime)
{
	// The detection phase:
	// 1. For each ball, check for collision with other balls
	//    --> if collision, add to "contact points" from perspective of "outer" ball
	// 2. For each ball, check for collision with triangles (environment)
	//    --> if collision, add to "contact points" from perspective of ball
	//
	// 3. Go throught the contact points, calculating response (sphere-sphere or sphere-triangle)
	// 4. Sort responses by object, calculate average resulting response
	//    --> update object


	// Clear the contact list


	// Brute force narrow phase detection

	bool breakOut = false;
	for (int bombIndex = 0; bombIndex != ::g_vec_pGOs.size(); bombIndex++)
	{
		
		cGameObject* pBomb = ::g_vec_pGOs[bombIndex];
		if (pBomb->meshName == "Isosphere_xyz_InvertedNormals.ply" && pBomb->bIsUpdatedByPhysics)
		{
			for (float i = 20.f; i > -20.f; i -= 2.f)
			{
				if (!i==0)
				for (float j = -20.f; j < 20.f; j += 2.f)
				{
					glm::vec3 ballPosition(pBomb->x, pBomb->y, pBomb->z);
					glm::vec3 turretPosition1(i, 1.f, j);
					glm::vec3 turretPosition2(i, 0.f, j+2.f);
					glm::vec3 turretPosition3(i, 1.5f, j + 2.f);
					if (abs(glm::distance(ballPosition, turretPosition1)) < pBomb->radius || abs(glm::distance(ballPosition, turretPosition2)) < pBomb->radius || abs(glm::distance(ballPosition, turretPosition3)) < pBomb->radius)
					{
						pBomb->solid_R = 1.f;
						pBomb->solid_G = 0.f;
						pBomb->solid_B = 0.f;
						pBomb->bIsUpdatedByPhysics = false;
						breakOut = true;
					}
					if (breakOut)
					{
						break;
					}
				}
				if (breakOut)
				{
					break;
				}
			}
		}

	}
/*

	for (int bombIndex = 0; bombIndex != ::g_vec_pGOs.size(); bombIndex++)
	{
	
		cGameObject* pBomb = ::g_vec_pGOs[bombIndex];
		if (pBomb->meshName == "Isosphere_xyz_InvertedNormals.ply" && pBomb->bIsUpdatedByPhysics)
		{
			for (float i = 2.f; i > -20.f; i += 2.f)
			{
				for (float j = -20.f; j < 20.f; j += 2.f)
				{
					glm::vec3 ballPosition(pBomb->x, pBomb->y, pBomb->z);
					glm::vec3 turretPosition1(i, 1.f, j);
					glm::vec3 turretPosition2(i, 0.f, j + 2.f);
					if (abs(glm::distance(ballPosition, turretPosition1)) < pBomb->radius || abs(glm::distance(ballPosition, turretPosition2)) < pBomb->radius)
					{
						pBomb->solid_R = 1.f;
						pBomb->solid_G = 0.f;
						pBomb->solid_B = 0.f;
						pBomb->bIsUpdatedByPhysics = false;
						breakOut = true;
					}
					if (breakOut)
					{
						break;
					}
				}
				if (breakOut)
				{
					break;
				}
			}
		}

	}*/
	

	return;
}
//
void PhysicsStep(float deltaTime)		// 24 Hz  30 Hz  60 Hz  100 Hz 60Hz  50,000 fps
{
	//glm::vec3 gravityForce(0.0f, -9.81f, 0.0f );
	glm::vec3 gravityForce(0.0f, 0.0f, 0.0f );

	// Euler... 
	for ( int index = 0; index != ::g_vec_pGOs.size(); index++)
	{
		// f = m*a
		// Velocity = 1 m per second
		// x = x + velocity*time
		// v = v + accel*time

		// each second 
		cGameObject* pCurGO = ::g_vec_pGOs[index];

		if ( pCurGO->bIsUpdatedByPhysics )
		{
			// Update velocity based on acceleration

			// This: 
			//		pCurGO->Vx = pCurGO->Vx + ( pCurGO->Ax * deltaTime ) ;
			//
			// Would typically be expressed as this:
			//		pCurGO->Vx += pCurGO->Ax * deltaTime;
			//
			// With the "+=" operator
			if (pCurGO->meshName == "y-wing_N.ply")
			{
				pCurGO->Vx += (pCurGO->Ax) * deltaTime;
				pCurGO->Vy += (pCurGO->Ay) * deltaTime;
				pCurGO->Vz += (pCurGO->Az) * deltaTime;
			}
			else
			{
				
				pCurGO->Vx += (pCurGO->Ax + gravityForce.x) * deltaTime;
				if (pCurGO->Vx > 0)
					pCurGO->Vx = 0;
				pCurGO->Vy += (pCurGO->Ay + gravityForce.y) * deltaTime;
				pCurGO->Vz += (pCurGO->Az + gravityForce.z) * deltaTime;
				if (pCurGO->Vz > 0)
					pCurGO->Vz = 0;
			}
			pCurGO->lastX = pCurGO->x;
			pCurGO->lastY = pCurGO->y;
			pCurGO->lastZ = pCurGO->z;

			// Update position based on velocity
			pCurGO->x += pCurGO->Vx * deltaTime; 
			pCurGO->y += pCurGO->Vy * deltaTime; 
			pCurGO->z += pCurGO->Vz * deltaTime;
		}//if ( pCurShip->bIsUpdatedByPhysics )
	}

	return;
}

void ReleaseBomb()
{
	cGameObject* pBomb = new cGameObject();
	pBomb->bIsUpdatedByPhysics = true;	// 
	pBomb->bIsWireframe = false;
	// Pick a semi-random location
	pBomb->x = g_vec_pGOs[0]->x;		// getRandFloat(-30.0f, 30.0f);
	pBomb->y = g_vec_pGOs[0]->y-.3;		// getRandFloat(-30.0f, 30.0f);
	pBomb->z = g_vec_pGOs[0]->z;		// getRandFloat(-30.0f, 30.0f);
	pBomb->solid_R = 0.f;
	pBomb->solid_G = 0.3f;
	pBomb->solid_B = 1.f;
	pBomb->Vx = -15.34;
//	pBomb->Vz = -15.34;
	pBomb->Ax = 3.f;
	pBomb->Az = 3.f;
	//pTurret->post_Rot_Y = -2.f;

	pBomb->radius = .5f;
	pBomb->scale = .5f;
	pBomb->meshName = "Isosphere_xyz_InvertedNormals.ply";
	// Pick a random rotation
	//pPlanet->post_Rot_Y = getRandFloat( 0.0f, 0.0f );
	g_vec_pGOs.push_back(pBomb);
}


void LoadTurrets()
{
	float x1, x2, x3;
	float z1, z2, z3;

	//x1 = glm::linearRand(-3.f, -30.f);
	//x2 = glm::linearRand(-3.f, -30.f);
	//x3 = glm::linearRand(-3.f, -30.f);

	//z1 = 0.f;
	//z2 = 5.f;
	//z3 = 10.f;

	//turret1 = { x1, .45f,z1 };
	//turret2 = { x2, .45f,z2 };
	//turret3 = { x3, .45f,z3 };

	cGameObject* pTurret = new cGameObject();
	pTurret->bIsUpdatedByPhysics = false;	// 
	pTurret->bIsWireframe = false;
	// Pick a semi-random location
	pTurret->x = turret1.x;		// getRandFloat(-30.0f, 30.0f);
	pTurret->y = turret1.y;		// getRandFloat(-30.0f, 30.0f);
	pTurret->z = turret1.z;		// getRandFloat(-30.0f, 30.0f);
	pTurret->solid_R = 0.f;
	pTurret->solid_G = 1.f;
	pTurret->solid_B = 0.f;
	//pTurret->post_Rot_Y = -2.f;

	pTurret->radius = 1.5f;
	pTurret->scale = 3.f;
	pTurret->meshName = "imperial turret_N.ply";
	// Pick a random rotation
	//pPlanet->post_Rot_Y = getRandFloat( 0.0f, 0.0f );
	::g_vec_pGOs.push_back(pTurret);

	cGameObject* pTurret1 = new cGameObject();
	pTurret1->bIsUpdatedByPhysics = false;	// 
	pTurret1->bIsWireframe = false;
	// Pick a semi-random location
	pTurret1->x = turret2.x;		// getRandFloat(-30.0f, 30.0f);
	pTurret1->y = turret2.y;		// getRandFloat(-30.0f, 30.0f);
	pTurret1->z = turret2.z;		// getRandFloat(-30.0f, 30.0f);
	pTurret1->solid_R = 0.f;
	pTurret1->solid_G = 0.f;
	pTurret1->solid_B = 1.f;
	//pTurret->post_Rot_Y = -2.f;

	pTurret1->radius = 1.5f;
	pTurret1->scale = 3.f;
	pTurret1->meshName = "imperial turret_N.ply";
	// Pick a random rotation
	//pPlanet->post_Rot_Y = getRandFloat( 0.0f, 0.0f );
	::g_vec_pGOs.push_back(pTurret1);

	cGameObject* pTurret2 = new cGameObject();
	pTurret2->bIsUpdatedByPhysics = false;	// 
	pTurret2->bIsWireframe = false;
	// Pick a semi-random location
	pTurret2->x = turret3.x;		// getRandFloat(-30.0f, 30.0f);
	pTurret2->y = turret3.y;		// getRandFloat(-30.0f, 30.0f);
	pTurret2->z = turret3.z;		// getRandFloat(-30.0f, 30.0f);
	pTurret2->solid_R = 1.f;
	pTurret2->solid_G = 0.f;
	pTurret2->solid_B = 0.f;
	//pTurret->post_Rot_Y = -2.f;

	pTurret2->radius = 1.5f;
	pTurret2->scale = 3.f;
	pTurret2->meshName = "imperial turret_N.ply";
	// Pick a random rotation
	//pPlanet->post_Rot_Y = getRandFloat( 0.0f, 0.0f );
	::g_vec_pGOs.push_back(pTurret2);


}

void Networking()
{
	WSADATA wsaData;
	SOCKET ConnectSocket = INVALID_SOCKET;
	struct addrinfo *result = NULL;
	struct addrinfo *ptr = NULL;
	struct addrinfo hints;

	Buffer* sendBuffer = new Buffer(DEFAULT_BUFLEN);
	Buffer* receiveBuffer = new Buffer(DEFAULT_BUFLEN);
	std::string userName;
	std::string password;
	int mode;


	//while (userName.length() < 4) {
	//	std::cout << "Please enter your username (must be at least 5 characters): ";

	//	std::getline(std::cin, userName);
	//}
	//bool ratingPassed = false;
	//int skillRating;
	//while (!ratingPassed) {
	//	std::cout << "Please enter your rating (must be between 1 and 5000): ";

	//	std::cin >> skillRating;
	//	if (skillRating < 5000 && skillRating>0)
	//	{
	//		ratingPassed = true;
	//	}
	//}
	//int skillRating = rand() % 5000 + 1;
	mode = -1;
	//userName = userName + ": ";
	int gameID = -1;
	
	ComposeMessage(gameID, mode, sendBuffer);
	//  char *sendbuf = "this is a test";
	char recvbuf[DEFAULT_BUFLEN];
	int resultInt;
	int recvbuflen = DEFAULT_BUFLEN;

	//Step 1
	// Initialize Winsock
	resultInt = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (resultInt != 0)
	{
		std::cout << "WinSock Initalization failed" << std::endl;
		return;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	// Resolve the server address and port
	resultInt = getaddrinfo("127.0.0.1", DEFAULT_PORT, &hints, &result);
	if (resultInt != 0) {
		std::cout << "Socket Initalization failed" << std::endl;
		WSACleanup();
		return;
	}

	//std::cout << "Step 1: WinSock Initalized" << std::endl;

	// Attempt to connect to an address until one succeeds
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next)
	{

		// Create a SOCKET for connecting to server
		ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);

		if (ConnectSocket == INVALID_SOCKET)
		{
			std::cout << "Socket failed with error: " << WSAGetLastError() << std::endl;
			WSACleanup();
			return;
		}

		// Connect to server.
		resultInt = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);

		if (resultInt == SOCKET_ERROR)
		{
			closesocket(ConnectSocket);
			ConnectSocket = INVALID_SOCKET;
			continue;
		}
		break;
	}

	//std::cout << "Step 2: Socket Created" << std::endl; //Put this here so we only see the message once althought we may try multiple sockets
	//std::cout << "Step 3: Connected to the server" << std::endl;

	freeaddrinfo(result);

	if (ConnectSocket == INVALID_SOCKET)
	{
		std::cout << "Unable to connect to server!" << std::endl;
		WSACleanup();
		return;
	}


	// Send an initial buffer
	//ReadIncomingMessage(sendBuffer);
	const char* temp = sendBuffer->toCharArray();
	//	std::cout << sendBuffer->readStringBE(0,sendBuffer->returnBufferSize()) <<std::endl;
	resultInt = send(ConnectSocket, sendBuffer->toCharArray(), sendBuffer->returnBufferSize(), 0);
	if (resultInt == SOCKET_ERROR)
	{
		std::cout << "send failed with error: " << WSAGetLastError() << std::endl;
		closesocket(ConnectSocket);
		WSACleanup();
		return;
	}

	//std::cout << "Step 4: Sending / Receiving data" << std::endl;

	//std::cout << "Bytes Sent: " << resultInt << std::endl;


	// shutdown the connection since no more data will be sent

	std::string tempInput;
	mode = 2;
	//  Receive until the peer closes the connection
	//SOCKET ConnectSocket, Buffer* sendBuffer, Buffer* receiveBuffer, char recvbuf[], int recvbuflen
	std::thread receiveThread(ReceiveMessages, ConnectSocket, sendBuffer, receiveBuffer, recvbuf, recvbuflen);
	//gameDataLoaded = true;
	do
	{


	} while (resultInt > 0);

	// cleanup


	//Keep the window open
	//std::cout << "\nwaiting on exit";


	resultInt = shutdown(ConnectSocket, SD_SEND);
	if (resultInt == SOCKET_ERROR)
	{
		std::cout << "shutdown failed with error: " << WSAGetLastError() << std::endl;
		closesocket(ConnectSocket);
		WSACleanup();
		return;
	}
	closesocket(ConnectSocket);
	WSACleanup();

	//std::cout << "Step 5: Disconnect" << std::endl;
	return;
}


void ComposeMessage(int gameID, int mode, Buffer* sendBuffer)
{


	sendBuffer->clear();
	sendBuffer->writeShort16BE(0);
	sendBuffer->setWriteIndex(2);
	sendBuffer->writeIntBE(mode);
	sendBuffer->writeIntBE(gameID);
	sendBuffer->setWriteIndex(0);
	sendBuffer->writeShort16BE(sendBuffer->returnBufferSize());
	//	}

}


void ReceiveMessages(SOCKET ConnectSocket, Buffer* sendBuffer, Buffer* receiveBuffer, char recvbuf[], int recvbuflen)
{
	do
	{
		sendBuffer->setReadIndex(0);
		sendBuffer->setWriteIndex(0);
		int resultInt = 0;
		resultInt = recv(ConnectSocket, recvbuf, recvbuflen, 0);


		if (resultInt > 2)
		{

			receiveBuffer->clear();
			receiveBuffer->loadBuffer(recvbuf, resultInt);
			int length = receiveBuffer->readShort16BE();
			if (length > resultInt)
			{
				std::cout << " Transmission Incomplete." << std::endl;
				while (length < resultInt)
				{
					resultInt = recv(ConnectSocket, recvbuf, sizeof recvbuf, 0);
				}
			}
			
				std::cout << "Bytes Received: " << receiveBuffer->returnBufferSize() << std::endl;
				ReadIncomingMessage(receiveBuffer);
			
		}
		else if (resultInt == 0)
		{
			//	std::cout << "Connection closed" << std::endl;

		}
		else
			std::cout << "recv failed with error: " << WSAGetLastError() << std::endl;

		receiveBuffer->clear();
	} while (true);
}


void ReadIncomingMessage(Buffer* receiveBuffer)
{

	int packetlength, messageID, roomID = 0;
	long messageLength = 0;

	//std::string userName1;
	//std::string userName2;
	//std::string ipAddress;

	int gameID;
	int x1, y1, z1;
	int x2, y2, z2;
	int x3, y3, z3;


	receiveBuffer->setReadIndex(0);
	//std::string serializedChatPacket = receiveBuffer->readStringBE(52);*/
	packetlength = receiveBuffer->readShort16BE();
	int messageType = receiveBuffer->readIntBE();
	if (messageType == 1)
	{
		gameID = receiveBuffer->readIntBE();
		x1 = receiveBuffer->readIntBE();
		z1 = receiveBuffer->readIntBE();
		x2 = receiveBuffer->readIntBE();
		z2 = receiveBuffer->readIntBE();
		x3 = receiveBuffer->readIntBE();
		z3 = receiveBuffer->readIntBE();

		turret1.x = (float) -x1;
		turret1.y = .45f;
		turret1.z = (float)z1;

		turret2.x = (float)-x2;
		turret2.y = .45f;
		turret2.z = (float)z2;

		turret3.x = (float)-x3;
		turret3.y = .45f;
		turret3.z = (float)z3;
		gameDataLoaded = true;
		//int readLength = receiveBuffer->readShort16BE();
		//userName1 = receiveBuffer->readStringBE(readLength);
		//readLength = receiveBuffer->readShort16BE();
		//userName2 = receiveBuffer->readStringBE(readLength);
		//readLength = receiveBuffer->readShort16BE();
		//ipAddress = receiveBuffer->readStringBE(readLength);

		//std::cout << "Player 1: " << userName1 << std::endl;
		//std::cout << "Player 2: " << userName2 << std::endl;
		//std::cout << "IP Address: " << ipAddress << std::endl;
	}


	//receiveBuffer->setReadIndex(0);
	////std::string serializedChatPacket = receiveBuffer->readStringBE(52);*/
	//packetlength = receiveBuffer->readShort16BE();
	//receiveBuffer->setReadIndex(6);

	//std::string serializedChatPacket;
	//int messageLen = packetlength - 2;
//	serializedChatPacket = receiveBuffer->readStringBE(4, messageLen);//(packetlength -2);


}


void FinalizePositions()
{

}