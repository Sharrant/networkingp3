// cGameObject.h
#ifndef _cGameObject_HG_
#define _cGameObject_HG_

#include <string>

class cGameObject
{
public:
	cGameObject();

	float pre_Rot_X, pre_Rot_Y, pre_Rot_Z;		// Euler angles 
	float post_Rot_X, post_Rot_Y, post_Rot_Z;	// Euler angles 
	float scale;		// Relative to a unit scale

	// Used in Physics and in graphics
	float x, y, z;			// Position ("translation")
	float radius;
	float lastX, lastY, lastZ;			// ADDED
	int getID(void);					// ADDED
	int health;
private:
	int m_uniqueID;
	static int m_nextID;

public:

	float Vx, Vy, Vz;
	float Ax, Ay, Az;

	int meshID;	// Which model we are drawing
	std::string meshName;		// "bunny.ply" or whatever

	bool bIsWireframe;
	float solid_R, solid_G, solid_B;		// Solid RGB
	bool bUseDebugColours;
	bool bIsVisible;


	bool bIsUpdatedByPhysics;
};

	////  Spherical bounding object
	//float radius;

	//// AABB 
	//float deltaX, deltaY, deltaZ;
	//float minX, minY, minZ;
	//float maxX, maxY, maxZ;

	//// AABB cube
	//float distanceFromCentre;

#endif 
