#include "cGameObject.h"
#include "Utilities.h"	// For getRandFloat
#include <vector>

// variable is NOT in this file, but it's somewhere
// (i.e. the linker will "link" to it)
extern unsigned int g_DebugSphereID;

bool LoadObjectsIntoScene( std::vector< cGameObject* > &vec_pGOs )
{

	cGameObject* pYWing = new cGameObject();
	pYWing->bIsUpdatedByPhysics = false;	// 
	pYWing->bIsWireframe = false;
	pYWing->bIsVisible = true;
	// Pick a semi-random location
	pYWing->x = -15.f;		// getRandFloat(-30.0f, 30.0f);
	pYWing->y =4.f;		// getRandFloat(-30.0f, 30.0f);
	pYWing->z = 30.f;		// getRandFloat(-30.0f, 30.0f);
	pYWing->solid_R = 0.f;
	pYWing->solid_G = 1.f;
	pYWing->solid_B = 0.f;
	pYWing->post_Rot_Y = -2.f;
	pYWing->health = 25;
//	pYWing->Vx = -15.4f;
	pYWing->Vz = -15.4f;

	pYWing->radius = 1.5f;
	pYWing->scale = 1.f;
	pYWing->meshName = "y-wing_N.ply";
	// Pick a random rotation
	//pPlanet->post_Rot_Y = getRandFloat( 0.0f, 0.0f );
	vec_pGOs.push_back(pYWing);

	cGameObject* pYWing1 = new cGameObject();
	pYWing1->bIsUpdatedByPhysics = false;	// 
	pYWing1->bIsWireframe = false;
	pYWing1->bIsVisible = false;
	// Pick a semi-random location
	pYWing1->x = -15.f;		// getRandFloat(-30.0f, 30.0f);
	pYWing1->y = 4.f;		// getRandFloat(-30.0f, 30.0f);
	pYWing1->z = 30.f;		// getRandFloat(-30.0f, 30.0f);
	pYWing1->solid_R = 0.f;
	pYWing1->solid_G = 1.f;
	pYWing1->solid_B = 0.f;
	pYWing1->post_Rot_Y = -2.f;
	pYWing1->health = 25;
//	pYWing1->Vx = -15.4f;
	pYWing1->Vz = -15.4f;

	pYWing1->radius = 1.5f;
	pYWing1->scale = 1.f;
	pYWing1->meshName = "y-wing_N.ply";
	// Pick a random rotation
	//pPlanet->post_Rot_Y = getRandFloat( 0.0f, 0.0f );
	vec_pGOs.push_back(pYWing1);

	cGameObject* pYWing2 = new cGameObject();
	pYWing2->bIsUpdatedByPhysics = false;	// 
	pYWing2->bIsWireframe = false;
	pYWing2->bIsVisible = false;
	// Pick a semi-random location
	pYWing2->x = -15.f;		// getRandFloat(-30.0f, 30.0f);
	pYWing2->y = 4.f;		// getRandFloat(-30.0f, 30.0f);
	pYWing2->z = 30.f;		// getRandFloat(-30.0f, 30.0f);
	pYWing2->solid_R = 0.f;
	pYWing2->solid_G = 1.f;
	pYWing2->solid_B = 0.f;
	pYWing2->post_Rot_Y = -2.f;
	pYWing2->health = 25;
//	pYWing2->Vx = -15.4f;
	pYWing2->Vz = -15.4f;

	pYWing2->radius = 1.5f;
	pYWing2->scale = 1.f;
	pYWing2->meshName = "y-wing_N.ply";
	// Pick a random rotation
	//pPlanet->post_Rot_Y = getRandFloat( 0.0f, 0.0f );
	vec_pGOs.push_back(pYWing2);


	cGameObject* pYWing3 = new cGameObject();
	pYWing3->bIsUpdatedByPhysics = false;	// 
	pYWing3->bIsWireframe = false;
	pYWing3->bIsVisible = false;
	// Pick a semi-random location
	pYWing3->x = -15.f;		// getRandFloat(-30.0f, 30.0f);
	pYWing3->y = 4.f;		// getRandFloat(-30.0f, 30.0f);
	pYWing3->z = 30.f;		// getRandFloat(-30.0f, 30.0f);
	pYWing3->solid_R = 0.f;
	pYWing3->solid_G = 1.f;
	pYWing3->solid_B = 0.f;
	pYWing3->post_Rot_Y = -2.f;
	pYWing3->health = 25;
	//pYWing3->Vx = -15.4f;
	pYWing3->Vz = -15.4f;

	pYWing3->radius = 1.5f;
	pYWing3->scale = 1.f;
	pYWing3->meshName = "y-wing_N.ply";
	// Pick a random rotation
	//pPlanet->post_Rot_Y = getRandFloat( 0.0f, 0.0f );
	vec_pGOs.push_back(pYWing3);

	cGameObject* pYWing4 = new cGameObject();
	pYWing4->bIsUpdatedByPhysics = false;	// 
	pYWing4->bIsWireframe = false;
	pYWing4->bIsVisible = false;
	// Pick a semi-random location
	pYWing4->x = -15.f;		// getRandFloat(-30.0f, 30.0f);
	pYWing4->y = 4.f;		// getRandFloat(-30.0f, 30.0f);
	pYWing4->z = 30.f;		// getRandFloat(-30.0f, 30.0f);
	pYWing4->solid_R = 0.f;
	pYWing4->solid_G = 1.f;
	pYWing4->solid_B = 0.f;
	pYWing4->post_Rot_Y = -2.f;
	pYWing4->health = 25;
//	pYWing4->Vx = -15.4f;
	pYWing4->Vz = -15.4f;

	pYWing4->radius = 1.5f;
	pYWing4->scale = 1.f;
	pYWing4->meshName = "y-wing_N.ply";
	// Pick a random rotation
	//pPlanet->post_Rot_Y = getRandFloat( 0.0f, 0.0f );
	vec_pGOs.push_back(pYWing4);

				cGameObject* pTrench = new cGameObject();
				pTrench->bIsUpdatedByPhysics = false;	// 
				pTrench->bIsWireframe = false;
				// Pick a semi-random location
				pTrench->x = 0;		// getRandFloat(-30.0f, 30.0f);
				pTrench->y = 0;		// getRandFloat(-30.0f, 30.0f);
				pTrench->z = 0;		// getRandFloat(-30.0f, 30.0f);

				pTrench->solid_R = 1.f;
				pTrench->solid_G = 1.f;
				pTrench->solid_B = 1.f;

				pTrench->radius = 1.5f;
				pTrench->scale = 100.f;
				pTrench->meshName = "Trench_export_2.ply";
				// Pick a random rotation
				//pPlanet->post_Rot_Y = getRandFloat( 0.0f, 0.0f );
				vec_pGOs.push_back(pTrench);

		


				


				for (float i = 2.f; i < 20.f; i += 2.f)
				{
					for (float j = -20.f; j < 20.f; j += 2.f)
					{
						cGameObject* pTurret = new cGameObject();
						pTurret->bIsUpdatedByPhysics = false;	// 
						pTurret->bIsWireframe = false;
						// Pick a semi-random location
						pTurret->x = i;		// getRandFloat(-30.0f, 30.0f);
						pTurret->y = .45f;		// getRandFloat(-30.0f, 30.0f);
						pTurret->z = j;		// getRandFloat(-30.0f, 30.0f);
						pTurret->solid_R = 1.f;
						pTurret->solid_G = 1.f;
						pTurret->solid_B = 1.f;
						//pTurret->post_Rot_Y = -2.f;

						pTurret->radius = 1.5f;
						pTurret->scale = 1.f;
						pTurret->meshName = "imperial turret_N.ply";
						// Pick a random rotation
						//pPlanet->post_Rot_Y = getRandFloat( 0.0f, 0.0f );
						vec_pGOs.push_back(pTurret);


						


					}
				}

				//for (float i = -2.f; i > -20.f; i -= 2.f)
				//{
				//	for (float j = -20.f; j < 20.f; j += 2.f)
				//	{
				//		cGameObject* pTurret = new cGameObject();
				//		pTurret->bIsUpdatedByPhysics = false;	// 
				//		pTurret->bIsWireframe = false;
				//		// Pick a semi-random location
				//		pTurret->x = i;		// getRandFloat(-30.0f, 30.0f);
				//		pTurret->y = .45f;		// getRandFloat(-30.0f, 30.0f);
				//		pTurret->z = j;		// getRandFloat(-30.0f, 30.0f);
				//		pTurret->solid_R = 1.f;
				//		pTurret->solid_G = 1.f;
				//		pTurret->solid_B = 1.f;
				//		//pTurret->post_Rot_Y = -2.f;

				//		pTurret->radius = 1.5f;
				//		pTurret->scale = 1.f;
				//		pTurret->meshName = "imperial turret_N.ply";
				//		// Pick a random rotation
				//		//pPlanet->post_Rot_Y = getRandFloat( 0.0f, 0.0f );
				//		vec_pGOs.push_back(pTurret);
				//	}
				//}

				for (int i = 0; i < 10; i++)
				{
					cGameObject* pTargettingSphere = new cGameObject();
					pTargettingSphere->bIsUpdatedByPhysics = false;	// 
					pTargettingSphere->bIsWireframe = true;
					// Pick a semi-random location
					pTargettingSphere->x = 2000.f;		// getRandFloat(-30.0f, 30.0f);
					pTargettingSphere->y = 1.f;		// getRandFloat(-30.0f, 30.0f);
					pTargettingSphere->z = 0.f;		// getRandFloat(-30.0f, 30.0f);
					pTargettingSphere->solid_R = 0.f;
					pTargettingSphere->solid_G = 1.f;
					pTargettingSphere->solid_B = 0.f;
					//pTurret->post_Rot_Y = -2.f;

					pTargettingSphere->radius = 1.5f;
					pTargettingSphere->scale = 1.5f;
					pTargettingSphere->meshName = "Isoshphere_xyz_InvertedNormals.ply";
					// Pick a random rotation
					//pPlanet->post_Rot_Y = getRandFloat( 0.0f, 0.0f );
					vec_pGOs.push_back(pTargettingSphere);
				}

	return true;
}