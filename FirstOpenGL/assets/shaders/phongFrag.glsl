#version 400

// FRAGMENT shader

uniform bool bUseDebugDiffuseNoLighting;

const int NUMBEROFLIGHTS = 10;

struct sLightDesc
{
	vec4 position;		// In the world
	vec4 diffuse;		// 1,1,1 (white colour light)
	vec4 attenuation;	// x = const, y = linear, z = quad, w = specular ratio
	vec4 specular;		// w is specular power
	vec4 typeAndParams;	// To be used soon. Specify type and attributes of type
};

uniform sLightDesc theLights[NUMBEROFLIGHTS];

// For spec:
uniform vec3 eyeLocation;

// In from the vertex shader
in vec4 diffuseColour;	
in vec3 vertMV;			// view vector
in vec3 vertWorld;
in vec3 vertNorm;
//in vec3 viewVector;
in vec3 lightVector[NUMBEROFLIGHTS];
in vec3 lightHalfVector[NUMBEROFLIGHTS];


// out to the framebuffer
out vec4 out_Colour;

// This was taken out of the main portion. Just the light calculation
vec3 calcLightContribution( vec3 diffuseObjectColour, vec3 vecView, int lightIndex )
{
	// Can be used as an early exit for the shader
	// (if you add a "distance" value to each light, the distance 
	//  being how far the light has any influence. You'd set this to 
	//  a place where the light is essentially attenuated to "zero")
	float distanceFromLight = length(lightVector[lightIndex].xyz); 
	// Option: Add if statement to early exit if distance is "too far"
	
	// The final attenuation is calculated. This is done here so 
	//  that you can also, optionally, early exit, say if the 
	//  attenuation is something very high (making the light 
	//  contribution essentially zero).
	float atten = 1.0f /( theLights[lightIndex].attenuation.x + 
	                      theLights[lightIndex].attenuation.y * distanceFromLight +
						  theLights[lightIndex].attenuation.z * distanceFromLight * distanceFromLight );

	
	// Normalize the light vector (which is the ray from the 
	//  vertex (world space) to the light (world space).
	// Since it's normalized, we only care about the
	//  direction, so essentially this is like a ray 
	//  cast from the origin (like the "normal" vector).
	vec3 lightVec = normalize(lightVector[lightIndex].xyz);
	
	// Diffuse contribution is the amount of light being reflected 
	//  (++INDEPENDENT of the eye (view) location++)
	//  from the LIGHT and the normal. Recal:
	//  - "lightVector" is ray from vertex (world space) to light (world space), normalized
	//  - "vertNormal" is normal of vertex (world space) with no translation
	// So both vectors are effectively coming from the origin (are only directions)
 	vec3 diffuse = max(0.0, dot(vertNorm.xyz, lightVec)) * theLights[lightIndex].diffuse.xyz * diffuseColour.xyz ;
	// * diffuseObjectColour;
				   //* theLights[lightIndex].diffuse.xyz;
				   	
	diffuse *= atten;
	
	// Specular: Phong
	// We do a simliar thing to the diffuse, but this time we are 
	//  seeing how much light is being reflected into THE EYE.
	// With diffuse, we don't care about the eye, as the colour
	//  we get is the same from every angle - it's only altered
	//  based on the light and normal position. 
	// The "most" light is reflected when the eye is directly
	//  in the path of the reflection vector, so let's calculate 
	//  that, first:
	//
	// The "view", "eye", or "camera" vector is the ray cast from 
	// 	the vertex, in "world" space, to the eye (also in "world" space).
	vec3 viewVector = normalize(eyeLocation - vertWorld);
	// The "light vector" is the ray from the vertex to the light.
	// (we've already calculated that)
	// 
	//  lightVector[index].xyz = theLights[index].position.xyz - vertWorld.xyz; (in VS)
	//  ---and---
	//	vec3 lightVec = normalize(lightVector[lightIndex].xyz); (FS: line 60)
	// 
	// Calculate the reflection vector from the light around normal:
	vec3 vecReflect = normalize(reflect(-lightVec,vertNorm));
	// The closer the angle between the eye and the vertex (the "viewVector") 
	//  and this reflection vector, the "brighter" the light is.
	//
	float specReflect = max(dot(vecReflect, viewVector), 0.0f);	// Silimar to diffuse, but uses view
	
	float lightSpecShinniness = theLights[lightIndex].specular.w;
	lightSpecShinniness = 1000.0f;
	float specPower = pow( specReflect, lightSpecShinniness );
//	vec3 specular = specPower * theLights[lightIndex].specular.xyz;
	vec3 specular = specPower * vec3(1.0f, 1.0f, 1.0f);
	specular *= atten;
	specular = clamp(specular, 0.0f, 1.0f);


//	return diffuse;
//	return specular;
//	return specReflect * vec3(1.0f, 1.0f, 1.0f);
//	return specReflect * vec3(1.0f, 1.0f, 1.0f) * atten;
	
	vec3 result = clamp(diffuse + specular, 0.0, 1.0f);
	
	return result;
}

void main()
{
	// Check to see if we are going to do the lighthing calculation
	if ( bUseDebugDiffuseNoLighting )
	{	// Bypass lighting entirely and set to uniform "debug" diffuse
		out_Colour.xyz = diffuseColour.xyz;
		out_Colour.a = 1.0f;
		return;
	}

	vec3 vecView = normalize(vertMV);
	
	// Do lighting calculation per light
	vec3 lightContrib = vec3(0.0f, 0.0f, 0.0f);
	for ( int index = 0; index != NUMBEROFLIGHTS; index++ )
	{
		lightContrib += calcLightContribution( diffuseColour.xyz, vecView, index );
	}
	
	
	// Bump up the brightness (projector is too dark)
//	diffuse.xyz *= vec3(1.5f);
	
	out_Colour = vec4(lightContrib, 1.0f);	
	
//	out_Colour *= 0.001f;
	
//	out_Colour += diffuseColour;
	
}