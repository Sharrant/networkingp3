#version 400

// FRAGMENT shader

uniform bool bUseDebugDiffuseNoLighting;

const int NUMBEROFLIGHTS = 1;

struct sLightDesc
{
	vec4 position;		// In the world
	vec4 diffuse;		// 1,1,1 (white colour light)
	vec4 attenuation;	// x = const, y = linear, z = quad, w = specular ratio
	vec4 specular;		// w is specular power
	vec4 typeAndParams;	// To be used soon. Specify type and attributes of type
};

uniform sLightDesc theLights[NUMBEROFLIGHTS];

// For spec:
uniform vec3 eyeLocation;

// In from the vertex shader
in vec3 diffuseColour;
in vec4 vertWorldXYZ;
in vec4 vertNorm;
in vec3 viewVector;
in vec3 lightVector[NUMBEROFLIGHTS];
in vec3 lightHalfVector[NUMBEROFLIGHTS];


// out to the framebuffer
out vec4 out_Colour;

// This was taken out of the main portion. Just the light calculation
vec3 calcLightContribution( vec3 diffuseObjectColour, vec3 vecView, int lightIndex )
{
//	vec3 vertNormal = normalize(vertNorm.xyz);					// Could be taken out of function
	vec3 lightVec = normalize(lightVector[lightIndex].xyz);
//	vec3 viewVector = normalize(viewVector);					// Could be taken out of function

	// Done in vertex shader, now
	//vec3 lightv = theLights[lightIndex].position.xyz - vertWorldXYZ.xyz;
	
	float distanceFromLight = length(lightVector[lightIndex]); 

	float atten = 1.0f /( theLights[lightIndex].attenuation.x + 
	                      theLights[lightIndex].attenuation.y * distanceFromLight +
						  theLights[lightIndex].attenuation.z * distanceFromLight * distanceFromLight );
	
	vec3 lightv = normalize(lightVector[lightIndex]);
	
 	vec3 diffuse = max(0.0, dot(vertNorm.xyz, lightv)) 
                   * diffuseObjectColour
				   * theLights[lightIndex].diffuse.xyz;
				   	
	diffuse *= atten;
	
	// Specular: Phong
	vec3 vecReflect = reflect(-lightv, vertNorm.xyz);

	//	From Vertex Shader...
//	viewVector = eyeLocation - vertWorldXYZ.xyz;	// View vector

	
	float specReflect = max(dot(vecReflect, vecView), 0.0f);	// Silimar to diffuse, but uses view
//	float specPower = pow( specReflect, theLights[lightIndex].specular.w );
	float specPower = pow( specReflect, 1.0f );
	
	vec3 specular = specPower * vec3(0.2f, 0.2f, 0.2f);
	specular *= atten;

//	return (diffuse + specular) * atten;
//	return diffuse;
	return specular;
}

void main()
{
	// Check to see if we are going to do the lighthing calculation
	if ( bUseDebugDiffuseNoLighting )
	{	// Bypass lighting entirely and set to uniform "debug" diffuse
		out_Colour.xyz = diffuseColour.xyz;
		out_Colour.a = 1.0f;
		return;
	}

	vec3 vecView = normalize(viewVector);
	
	// Do lighting calculation per light
	vec3 diffuse = vec3(0.0f, 0.0f, 0.0f);
	for ( int index = 0; index != NUMBEROFLIGHTS; index++ )
	{
		diffuse += calcLightContribution( diffuseColour.xyz, vecView, index );
	}
	
	
	// Bump up the brightness (projector is too dark)
//	diffuse.xyz *= vec3(1.5f);
	
	out_Colour = vec4(diffuse, 1.0f);	
	
//	out_Colour *= 0.001f;	// make it (essentially) zero.	
//	out_Colour += vec4(myLightDiffuse.xyz, 1.0f);
}