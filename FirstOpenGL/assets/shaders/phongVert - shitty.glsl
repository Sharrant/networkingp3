#version 400

// VAO layout (vertex attribute layout)
in vec3 vColour;		
in vec3 vPosition;	
in vec3 vNormal;

uniform mat4 matModel;	// "per object" 
uniform mat4 matView;	// "per scene" or "per frame"
uniform mat4 matProj;	// "Projection" (only if size or aspect changes)

uniform vec3 objectColour;
uniform int isWireframe;		



const int NUMBEROFLIGHTS = 1;

struct sLightDesc
{
	vec4 position;		// In the world
	vec4 diffuse;		// 1,1,1 (white colour light)
	vec4 attenuation;	// x = const, y = linear, z = quad, w = not used
	vec4 typeAndParams;	// To be used soon. Specify type and attributes of type
};

uniform sLightDesc theLights[NUMBEROFLIGHTS];

// For spec:
uniform vec3 eyeLocation;

out vec3 diffuseColour;		
out vec4 vertWorldXYZ;
out vec4 vertNorm;
out vec3 viewVector;
out vec3 lightVector[NUMBEROFLIGHTS];
out vec3 lightHalfVector[NUMBEROFLIGHTS];

// VERTEX shader
void main()
{
	mat4 matMV = matView * matModel;	// model-view matrix
	vec4 pMV = matMV * vec4(vPosition, 1.0f);

	mat4 matMVP = matProj * matView * matModel;		// m = p * v * m;
	
    gl_Position = matMVP * vec4( vPosition, 1.0f );
	
	vertWorldXYZ = matModel * vec4( vPosition, 1.0f );
	
	// Strip out scaling and tranlation part of the matrix...
//	mat4 modelIT = inverse( transpose(matModel) );	
//	vertNorm     = modelIT * vec4( normalize(vNormal), 1.0f );
//  OR
//	mat4 modelRotOnly = mat4(mat3(matModel), 1.0f );
	
	// Normal in view space
	vertNorm = vec4(normalize(vNormal.xyz), 1.0f);
	
	// Strip translation and scaling from model matrix.
	mat3 matInvTranWorld = mat3(inverse( transpose(matModel) ));
	
//	vertNorm.xyz = mat3(matModel) * vertNorm.xyz;
	vertNorm.xyz = mat3(inverse( transpose(matModel) )) * vertNorm.xyz;
	vertNorm.w = 1.0f;
	
	// Light vectors
	for ( int index = 0; index < NUMBEROFLIGHTS; index++ )
	{
//		lightVector[index] = theLights[index].position.xyz - pMV.xyz;
		lightVector[index] = theLights[index].position.xyz - vertWorldXYZ.xyz;
	}
	
//	viewVector = -pMV.xyz;	// View vector
	viewVector = normalize(eyeLocation - vertWorldXYZ.xyz);	// View vector
	
	if ( isWireframe == 1 )  
	{
		diffuseColour = objectColour;
	}
	else  
	{	
		diffuseColour = vColour;
	}
}