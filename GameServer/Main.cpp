#undef UNICODE

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <iphlpapi.h>
#include <stdio.h>
#include <iostream>
#include <thread>
#include <time.h>
#include "Buffer.h"


// for windows socket link
#pragma comment(lib, "Ws2_32.lib")

#define DEFAULT_PORT "27015"
#define DEFAULT_BUFLEN 512


void ReadIncomingMessage(Buffer* receiveBuffer);
void ComposeStartMessage(int gameID, int x1, int x2, int x3, int z1, int z2, int z3, Buffer* sendBuffer);

struct sGame
{
	float turret1x, turret1z;
	float turret2x, turret2z;
	float turret3x, turret3z;
	bool gameStart = false;
	int gameSocket;
};

int gameCount = 0;
std::vector<sGame> gameList;
//std::vector<sUnderwatchPlayer> playerList;
time_t start;
//bool MatchRatings(sUnderwatchPlayer player1, sUnderwatchPlayer player2);
void GameService();
//void MatchFound(sUnderwatchPlayer player1, sUnderwatchPlayer player2);
fd_set master;    //Master list
fd_set read_fds;
SOCKET listeningSocket;
std::vector<Buffer*> messagequeue;
std::vector<int> messagequeuesocket;

int main()
{

	//Step 1
	//Initial winsock
	WSADATA wsaData; //Create a Windows Socket Application Data Object

	int resultInt;

	resultInt = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (resultInt != 0)
	{
		std::cout << "WinSock Initalization failed" << std::endl;
		return 1;
	}

	//Step 2
	//Create a Socket
	struct addrinfo *result = NULL;
	struct addrinfo *ptr = NULL;
	struct addrinfo hints;

	ZeroMemory(&hints, sizeof(hints)); //Fills a block of memory with zeros
	hints.ai_family = AF_INET; //Unspecified so either IPv4 or IPv6 address can be returned
	hints.ai_socktype = SOCK_STREAM; //Stream must be specified for TCP
	hints.ai_protocol = IPPROTO_TCP; //Protocol is TCP
	hints.ai_flags = AI_PASSIVE;


	resultInt = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
	if (resultInt != 0)
	{
		std::cout << "Socket Initalization failed" << std::endl;
		WSACleanup(); //will nicely kill our WinSock instance
		return 1;
	}

	std::cout << "Step 1: WinSock Initalized" << std::endl;

	//Copy the result object pointer
	ptr = result;

	listeningSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);

	if (listeningSocket == INVALID_SOCKET)
	{
		std::cout << "Socket Initalization failed" << std::endl;
		freeaddrinfo(result); //free memory allocated to provent memory leaks
		WSACleanup(); //will nicely kill our WinSock instance
		return 1;
	}
	srand(time(0));


	/*NEW STUFF OCT 3RD */

	//We need to switch the socket to be non blocking
	// If iMode!=0, non-blocking mode is enabled.
	u_long iMode = 1;
	ioctlsocket(listeningSocket, FIONBIO, &iMode);

	/****/


	std::cout << "Step 2: Socket Created" << std::endl;

	//Step3 
	//Bind the Socket

	resultInt = bind(listeningSocket, result->ai_addr, (int)result->ai_addrlen);

	if (listeningSocket == INVALID_SOCKET)
	{
		std::cout << "Socket binding failed" << std::endl;
		freeaddrinfo(result); //free memory allocated to provent memory leaks
		closesocket(listeningSocket); //Close the socket
		WSACleanup(); //will nicely kill our WinSock instance
		return 1;
	}

	freeaddrinfo(result); //Once bind is called the address info is no longer needed so free the memory allocated

	std::cout << "Step 3: Socket Bound" << std::endl;

	//Step 4
	//Listen for a client connection

	if (listen(listeningSocket, SOMAXCONN) == SOCKET_ERROR)
	{
		std::cout << "Socket listening failed" << std::endl;
		closesocket(listeningSocket); //Close the socket
		WSACleanup(); //will nicely kill our WinSock instance
		return 1;
	}

	std::cout << "Step 4: Listening on Socket" << std::endl;


	/*NEW STUFF OCT 3RD */

	//Set up file descriptors for select statement
	//Temp list for select() statement

	//Clear the master and temp sets
	FD_ZERO(&master);
	FD_ZERO(&read_fds);

	//Add the listener to the master set
	FD_SET(listeningSocket, &master);

	//Keep track of the biggest file descriptor
	int newfd;        //Newly accepted socket descriptor
	int fdmax = listeningSocket; //Maximum file descriptor number (to start it's the listener)
	std::cout << "fdmax " << fdmax << std::endl;

	//Set timeout time
	struct timeval tv;
	tv.tv_sec = 0;
	tv.tv_usec = 500 * 1000; // 500 ms
	bool authServerAttached = false;
	int authServerNumber = 0;
	//std::thread MatchmakerThread(MatchMaker);
	//Main loop
	std::thread gameThread(GameService);
	start = time(0);
	while (true) //infinite loop, this could be scary
	{

		//Select function checks to see if any socket has activity
		read_fds = master; // copy the master to a temp
		if (select(fdmax + 1, &read_fds, NULL, NULL, &tv) == -1)
		{
			std::cout << "Select Error" << std::endl;
			exit(4);
		}

		//Loop through existing connections looking for data to read
		for (int i = 0; i <= fdmax; i++)
		{
			//If no flag is raised keep looping
			if (!FD_ISSET(i, &read_fds))
			{
				continue;
			}

			//If the raised flag is on the listening socket accept a new connection then keep looping
			if (i == listeningSocket)
			{

				// handle new connections
				sockaddr_in client;
				socklen_t addrlen = sizeof sockaddr_storage; //this represents the client address size
				newfd = accept(listeningSocket, (struct sockaddr *)&client, &addrlen);
				std::cout << "Connected to: " << client.sin_addr.s_addr << std::endl;

				std::cout << "newfd " << newfd << std::endl;
				if (newfd == -1)
				{
					std::cout << "Accept Error" << std::endl;
					continue;
				}

				FD_SET(newfd, &master); // add to master set

										//Keep track of max fd
				if (newfd > fdmax)
					fdmax = newfd;

				std::cout << "New connection on socket " << newfd << std::endl;
				continue;
			}

			/////////////////////////////////
			// Recieve an incoming message //
			/////////////////////////////////
			Buffer* receiveBuffer = new Buffer(DEFAULT_BUFLEN);
			char recvbuf[DEFAULT_BUFLEN];
			/*	for (int i = 0; i < DEFAULT_BUFLEN;i++)
			recvbuf[i] = { 0 };*/
			int incomingMessageLength = 0;

			//Recieve the message
			//ZeroMemory(recvbuf, 512);
			incomingMessageLength = recv(i, recvbuf, sizeof recvbuf, 0);

			if (incomingMessageLength > 2)
			{
				std::cout << "Bytes received: " << incomingMessageLength << std::endl;

				//std::cout << "Recieved Message: " << recvbuf << std::endl;
				receiveBuffer->clear();



				receiveBuffer->loadBuffer(recvbuf, incomingMessageLength);
				receiveBuffer->setReadIndex(0);
				int length = receiveBuffer->readShort16BE();
				if (length > incomingMessageLength)
				{
					std::cout << " Transmission Incomplete." << std::endl;
					while (length < incomingMessageLength)
					{
						incomingMessageLength = recv(i, recvbuf, sizeof recvbuf, 0);
					}
				}
				//	i--;
				
			//	if (incomingMessageLength>0)
				
					std::cout << "Bytes Received: " << receiveBuffer->returnBufferSize() << std::endl;
					//ReadIncomingMessage(receiveBuffer);


					int packetlength, messageID, commandID, roomID, messageLength = 0;
					packetlength = receiveBuffer->readShort16BE();

					messageID = receiveBuffer->readIntBE();
					if (messageID == -1)
					{
						sGame game;
						game.gameSocket = i;
						srand(time(0));
						game.turret1x = rand() % 30 + 1;
						game.turret2x = rand() % 30 + 1;
						game.turret3x = rand() % 30 + 1;
						game.turret1z = 5;
						game.turret2z = 10;
						game.turret3z = 15;
						game.gameStart = false;
						gameList.push_back(game);
						receiveBuffer->clear();

					}
				

			}
			else if (incomingMessageLength == 0)
			{
				std::cout << "Connection closing..." << std::endl;
				closesocket(i); //CLose the socket
				FD_CLR(i, &master); // remove from master set

				continue;
			}
			else
			{
				std::cout << "recv failed: " << WSAGetLastError() << std::endl;
				closesocket(i);
				WSACleanup();

				//You probably don't want to stop the server based on this in the real world
				return 1;
			}
		}
	}

	/****/


	//Step 7
	//Disconnect and Cleanup

	// shutdown the send half of the connection since no more data will be sent
	resultInt = shutdown(listeningSocket, SD_SEND);
	if (resultInt == SOCKET_ERROR)
	{
		std::cout << "shutdown failed:" << WSAGetLastError << std::endl;
		closesocket(listeningSocket);
		WSACleanup();
		return 1;
	}

	//Final clean up
	closesocket(listeningSocket);
	WSACleanup();

	std::cout << "Step 7: Disconnect" << std::endl;

	//Keep the window open
	std::cout << "\nwaiting on exit";
	int tempInput;
	std::cin >> tempInput;

	return 0;
}

void ReadIncomingMessage(Buffer* receiveBuffer)
{
	//int packetlength, messageID, roomID = 0;
	//long messageLength = 0;
	//std::string message;
	//std::string username;
	//receiveBuffer->setReadIndex(0);
	////std::string serializedChatPacket = receiveBuffer->readStringBE(52);*/
	//packetlength = receiveBuffer->readShort16BE();
	//receiveBuffer->setReadIndex(6);

	//std::string serializedChatPacket;
	//int messageLen = packetlength - 2;
	//serializedChatPacket = receiveBuffer->readStringBE(4, messageLen);//(packetlength -2);


	////	chatPacket.
	//std::string serializedWebAuth;


	//std::cout << createWeb.email() << " " << createWeb.requestid() << " " << createWeb.plaintextpassword << std::endl;
	//messageID = receiveBuffer->readIntBE();
	//	messageID = chatPacket.messageid();
	//	//roomID = receiveBuffer->readIntBE();
	//	//messageLength = receiveBuffer->readIntBE();
	//	message = chatPacket.message();
	//	messageLength = message.length();
	//	//receiveBuffer->addToReadIndex(2);
	//	//message = receiveBuffer->readStringBE(messageLength-2);
	//	username = chatPacket.username();
	//
	//
	////	std::cout << "packet length: " << packetlength << std::endl;
	//	std::cout << "message id: " << chatPacket.messageid() << std::endl;
	////	std::cout << "roomID: " << roomID << std::endl;
	//	std::cout << "message length: " << message.length() << std::endl;
	//	std::cout << "username: " << chatPacket.username() << std::endl;
	//	std::cout << "message: " << chatPacket.message() << std::endl;
}



void ComposeStartMessage(int gameID, int x1, int x2, int x3, int z1, int z2, int z3 , Buffer* sendBuffer)
{

	sendBuffer->clear();
	sendBuffer->writeShort16BE(0);
	sendBuffer->setWriteIndex(2);
	sendBuffer->writeIntBE(1);
	sendBuffer->writeIntBE(gameID);
	sendBuffer->writeIntBE(x1);
	sendBuffer->writeIntBE(z1);
	sendBuffer->writeIntBE(x2);
	sendBuffer->writeIntBE(z2);
	sendBuffer->writeIntBE(x3);
	sendBuffer->writeIntBE(z3);
	
	sendBuffer->setWriteIndex(0);
	sendBuffer->writeShort16BE(sendBuffer->returnBufferSize());


}

void ReceiveMessages(SOCKET ConnectSocket, Buffer* sendBuffer, Buffer* receiveBuffer, char recvbuf[], int recvbuflen)
{
	do
	{
		sendBuffer->setReadIndex(0);
		sendBuffer->setWriteIndex(0);
		int resultInt = 0;
		resultInt = recv(ConnectSocket, recvbuf, recvbuflen, 0);


		if (resultInt > 0)
		{
			receiveBuffer->clear();
			//std::cout << "Bytes received: " << resultInt << std::endl;
			receiveBuffer->loadBuffer(recvbuf, resultInt);
			/*receiveBuffer->setReadIndex(0);
			receiveBuffer->setWriteIndex(0);
			std::string check1 = receiveBuffer->toCharArray();
			std::string check2 = sendBuffer->toCharArray();
			int checkMessageId1 = receiveBuffer->readIntBE(2);
			int checkMessageId2 = sendBuffer->readIntBE(2);
			if (check1 != check2 && checkMessageId1!= checkMessageId2)*/
			//std::cout << receiveBuffer->readStringBE(resultInt);
		//	ReadIncomingMessage(receiveBuffer);
		}
		else if (resultInt == 0)
		{
			//	std::cout << "Connection closed" << std::endl;

		}
		else
			std::cout << "recv failed with error: " << WSAGetLastError() << std::endl;


	} while (true);
}

void GameService()
{
	
	
	while (true)
	{
		int size = gameList.size();
		// std::cout << "Player List Size: " << playerList.size() << std::endl;
		for (unsigned int i = 0; i < size; i++)
		{
			if (gameList[i].gameStart == false)
			{
				srand(time(0));
				int x1, x2, x3, z1, z2, z3;
				x1 = gameList[i].turret1x;
				x2 = gameList[i].turret2x;
				x3 = gameList[i].turret3x;
				z1 = gameList[i].turret1z;
				z2 = gameList[i].turret2z;
				z3 = gameList[i].turret3z;
				gameList[i].gameStart = true;
				Buffer* sendBuffer = new Buffer(DEFAULT_BUFLEN);
				ComposeStartMessage(gameCount,x1,x2,x3,z1,z2,z3,sendBuffer);


				//messagequeue.push_back(sendBuffer);
				//messagequeuesocket.push_back(gameList[i].gameSocket);
				//sGame game = gameList[i];
				for (int unsigned j = 0; j < master.fd_count; j++)
				{
					if (master.fd_array[j] == gameList[i].gameSocket)
					{
						//	if (i != j) {
						int jSendResult = send(master.fd_array[j], sendBuffer->toCharArray(), sendBuffer->returnBufferSize(), 0);

						if (jSendResult == SOCKET_ERROR)
						{
							std::cout << "Send failed: " << WSAGetLastError() << std::endl;
							//closesocket(i);
							//WSACleanup();
							//return 1;
						}
						std::cout << "Bytes sent: " << jSendResult << std::endl;
						//	}
					}

				}

				gameList.pop_back();

				//}
				sendBuffer->clear();
			//	int jSendResult = send(master.fd_array[gameList[i].gameSocket], sendBuffer->toCharArray(), sendBuffer->returnBufferSize(), 0);
			//	std::cout << "Bytes sent: " << sendBuffer->returnBufferSize() << std::endl;
			}
		}

	}
}

void MessageSender()
{
	while (true)
	{
		int size = messagequeue.size();
		for (int i = 0; i < size; i++)
		{
			
		}
	}
}


//
//void MatchFound(sUnderwatchPlayer player1, sUnderwatchPlayer player2)
//{
//	Buffer* sendBuffer = new Buffer(DEFAULT_BUFLEN);
//	sendBuffer->clear();
//	std::string ipAddress = "";
//	int size = gameServerList.size();
//	int lowestPop;
//	if (!gameServerList.empty())
//		lowestPop = gameServerList[0].numPlayers;
//	ipAddress = gameServerList[0].ipAddress;
//	int count = 0;
//	for (unsigned int i = 1; i < size; i++)
//	{
//		if (lowestPop > gameServerList[i].numPlayers)
//		{
//			lowestPop = gameServerList[i].numPlayers;
//			ipAddress = gameServerList[i].ipAddress;
//			count = i;
//		}
//	}
//	ComposeMessage(player1.userName, player2.userName, ipAddress, 1, sendBuffer);
//	gameServerList[count].numPlayers++;
//	for (int unsigned j = 0; j < master.fd_count; j++)
//	{
//		if (master.fd_array[j] == player1.socket)
//		{
//			//	if (i != j) {
//			int jSendResult = send(master.fd_array[j], sendBuffer->toCharArray(), sendBuffer->returnBufferSize(), 0);
//
//			if (jSendResult == SOCKET_ERROR)
//			{
//				std::cout << "Send failed: " << WSAGetLastError() << std::endl;
//				//closesocket(i);
//				//WSACleanup();
//				//return 1;
//			}
//			std::cout << "Bytes sent: " << jSendResult << std::endl;
//			//	}
//		}
//
//	}
//	ComposeMessage(player2.userName, player1.userName, ipAddress, 1, sendBuffer);
//	for (int unsigned j = 0; j < master.fd_count; j++)
//	{
//		if (master.fd_array[j] == player2.socket)
//		{
//			//	if (i != j) {
//			int jSendResult = send(master.fd_array[j], sendBuffer->toCharArray(), sendBuffer->returnBufferSize(), 0);
//
//			if (jSendResult == SOCKET_ERROR)
//			{
//				std::cout << "Send failed: " << WSAGetLastError() << std::endl;
//				//closesocket(i);
//				//WSACleanup();
//				//return 1;
//			}
//			std::cout << "Bytes sent: " << jSendResult << std::endl;
//			//	}
//		}
//
//	}
//
//}